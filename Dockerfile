FROM rust AS build
RUN apt-get update && \
    apt-get install -y protobuf-compiler npm && \
    cargo install dioxus-cli

WORKDIR /src
COPY . .
RUN cargo build --release --package server && \
    cd ui && \
    npm install && \
    npx tailwindcss -i ./input.css -o ./assets/tailwind.css && \
    dx build --release


FROM debian:bookworm-slim AS bin
WORKDIR /app
COPY --from=build /src/target/release/server ./
COPY --from=build /src/config.yaml ./
COPY --from=build /src/ui/dist ./ui/dist/
ENTRYPOINT ./server
