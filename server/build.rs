fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("../proto/query.proto")?;
    tonic_build::compile_protos("../proto/flag_configuration.proto")?;
    Ok(())
}
