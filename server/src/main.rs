mod app_config;
mod flags_synchronization;
mod query;
mod servers;
mod flag_storage;
mod group_version;

use crate::flag_storage::FlagStorage;
use crate::flags_synchronization::FlagsSynchronizer;
use crate::query::executor::Executor;
use anyhow::Result;
use app_config::AppConfig;
use config::{Config, Environment, File};
use log::error;
use servers::grpc::grpc_server;
use servers::http::http_server;
use std::sync::Arc;
use tracing_subscriber::EnvFilter;

#[tokio::main]
async fn main() -> Result<()> {
    let app_config = load_config();

    // initialize logs
    let subscriber_builder = tracing_subscriber::fmt()
        .with_target(false)
        .with_line_number(false)
        .with_file(false)
        .with_env_filter(EnvFilter::from_default_env());
    if app_config.json_logs {
        subscriber_builder.json().init();
    } else {
        subscriber_builder.init();
    }

    let result = start(app_config).await;
    if let Err(err) = result {
        error!("{}", err);
    }

    Ok(())
}

async fn start(app_config: AppConfig) -> Result<()> {
    let flag_storage = FlagStorage::new(&app_config.mongodb_connection_string).await?;
    let flags_synchronizer = FlagsSynchronizer::new(flag_storage.clone());
    let (flags, groups) = flags_synchronizer.start(app_config.sync_interval).await?;
    let executor = Arc::new(Executor::new(Arc::clone(&flags)));

    let grpc = grpc_server::start(app_config.server.grpc_address, Arc::clone(&executor), flags, groups);
    let http = http_server::start(app_config.server.http_address, executor, flag_storage, flags_synchronizer);

    let (grpc, api) = tokio::join!(tokio::spawn(grpc), tokio::spawn(http));
    grpc??;
    api??;

    Ok(())
}

fn load_config() -> AppConfig {
    let builder = Config::builder()
        .add_source(File::with_name("config.yaml").required(false))
        .add_source(Environment::default().separator("__"));
    let cfg = builder.build().unwrap();
    cfg.try_deserialize().unwrap()
}
