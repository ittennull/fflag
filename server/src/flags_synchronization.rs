use crate::flag_storage::FlagStorage;
use crate::group_version::GroupVersions;
use crate::query::types::FlagsMap;
use anyhow::Result;
use log::{debug, error};
use std::sync::Arc;
use std::time::Duration;
use tokio::sync::RwLock;
use tokio::time::sleep;

#[derive(Clone)] // to pass Self to a separate thread that updates data
pub struct FlagsSynchronizer {
    flags: Arc<RwLock<FlagsMap>>,
    groups: Arc<RwLock<GroupVersions>>,
    flag_storage: FlagStorage,
}

impl FlagsSynchronizer {
    pub fn new(flag_storage: FlagStorage) -> Self {
        Self {
            flags: Arc::new(RwLock::new(FlagsMap::default())),
            groups: Arc::new(RwLock::new(GroupVersions::default())),
            flag_storage,
        }
    }

    pub async fn start(&self, sync_interval: u64) -> Result<(Arc<RwLock<FlagsMap>>, Arc<RwLock<GroupVersions>>)> {
        self.refresh().await?;

        let clone = self.clone();
        tokio::spawn(async move {
            loop {
                sleep(Duration::from_secs(sync_interval)).await;

                if let Err(error) = clone.refresh().await {
                    error!("Sync failed: {}", error);
                }
            }
        });

        Ok((Arc::clone(&self.flags), Arc::clone(&self.groups)))
    }

    pub async fn refresh(&self) -> Result<()> {
        {
            let new_flags = load_flags(&self.flag_storage).await?;
            let mut flags_guard = self.flags.write().await;
            *flags_guard = new_flags;
        }
        {
            let groups = load_groups(&self.flag_storage).await?;
            let mut groups_guard = self.groups.write().await;
            *groups_guard = groups;
        }
        Ok(())
    }
}


async fn load_flags(flag_storage: &FlagStorage) -> Result<FlagsMap> {
    let flags = flag_storage.get_all_flags().await?;
    let flags_map: FlagsMap = flags.into_iter().map(|flag| (flag.name.clone(), flag)).collect();

    debug!("Loaded {} flags from the storage", flags_map.len());

    Ok(flags_map)
}

async fn load_groups(flag_storage: &FlagStorage) -> Result<GroupVersions> {
    let groups = flag_storage.get_all_groups().await?;
    let group_versions: GroupVersions = groups.into_iter().map(|x| (x.group_name.clone(), x)).collect();

    debug!("Loaded {} group versions from the storage", group_versions.len());

    Ok(group_versions)
}