use crate::query::types::FlagRequest;
use std::fmt::{Display, Formatter, Result};

pub struct DisplayFlagRequest<'a, T: FlagRequest>(pub &'a T);

impl<'a, T: FlagRequest> Display for DisplayFlagRequest<'a, T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{{name={}", self.0.flag_name())?;

        if let Some(entity_id) = self.0.entity_id() {
            write!(f, ", entityId={}", entity_id)?;
        }

        write!(f, "}}")
    }
}
