pub enum Check {
    IsEqualTo,
    IsNotEqualTo,
    GreaterThan,
    GreaterThanOrEqualTo,
    LessThan,
    LessThanOrEqualTo,
    In,
    NotIn,
}

pub fn check<T: PartialOrd>(x: &Option<T>, y: &T, check_type: Check) -> bool {
    x.as_ref().map_or(false, |x| match check_type {
        Check::IsEqualTo => x == y,
        Check::IsNotEqualTo => x != y,
        Check::GreaterThan => x > y,
        Check::GreaterThanOrEqualTo => x >= y,
        Check::LessThan => x < y,
        Check::LessThanOrEqualTo => x <= y,
        _ => panic!("Only scalar related checks are allowed"),
    })
}

pub fn check_vec<T: PartialOrd>(x: &Option<&T>, y: &[T], check: Check) -> bool {
    x.map_or(false, |x| match check {
        Check::In => y.contains(x),
        Check::NotIn => !y.contains(x),
        _ => panic!("Only Vec related checks are allowed"),
    })
}
