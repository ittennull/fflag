use crate::query::checks::{self, Check};
use crate::query::impl_display_for_flag_request::DisplayFlagRequest;
use crate::query::types::{FlagRequest, FlagsMap, GetFlagResponse, GetFlagResult, Value};
use anyhow::{anyhow, Context, Result};
use common::flag::{
    Condition, ConditionValue, Flag, Parameter, Percent, Variants,
    DISTRIBUTION_PERCENT_MAX,
};
use log::debug;
use rand::{rngs::ThreadRng, Rng};
use std::cell::RefCell;
use std::sync::Arc;
use tokio::sync::RwLock;
use xxhash_rust::xxh3::xxh3_64_with_seed;

pub struct Executor {
    flags: Arc<RwLock<FlagsMap>>,
}

impl Executor {
    pub fn new(flags: Arc<RwLock<FlagsMap>>) -> Self {
        Self {
            flags,
        }
    }

    pub async fn get_flag(&self, request: &impl FlagRequest) -> Result<Option<GetFlagResponse>> {
        debug!("get_flag {}", DisplayFlagRequest(request));

        let flags_guard = self.flags.read().await;
        if let Some(flag) = flags_guard.get(request.flag_name()) {
            if !flag.enabled {
                debug!("flag '{}' is disabled", request.flag_name());
                return Ok(None);
            }

            let value = process_request(flag, request)?;

            debug!(
                "Evaluation for {} resulted in {:?}",
                DisplayFlagRequest(request),
                value
            );

            if let Some(value) = value {
                return Ok(Some(GetFlagResponse { value }));
            }
        }

        debug!("flag '{}' is not found", request.flag_name());
        Ok(None)
    }

    pub async fn get_flags<'a, FR: FlagRequest>(
        &self,
        requests: &'a [FR],
    ) -> Result<Vec<GetFlagResult<'a>>> {
        let flags_guard = self.flags.read().await;

        // collect existing enabled flags together with the requests
        let requests_and_flags: Vec<(&FR, &Flag)> = requests
            .iter()
            .filter_map(|request| {
                flags_guard
                    .get(request.flag_name())
                    .filter(|flag| flag.enabled)
                    .map(|flag| (request, flag))
            })
            .collect();

        // evaluate requests
        let mut results = Vec::with_capacity(requests_and_flags.len());
        for (request, flag) in requests_and_flags {
            let value = process_request(flag, request)?;

            if let Some(value) = value {
                results.push(GetFlagResult {
                    name: request.flag_name(),
                    entity_id: request.entity_id().map(|x| x.as_str()),
                    value,
                })
            }
        }

        Ok(results)
    }
}

fn process_request<'a>(
    flag: &'a Flag,
    request: &'a impl FlagRequest,
) -> Result<Option<Value>> {
    let distributions = match_request_and_get_distribution(request, flag)
        .with_context(|| format!("Flag name {}", flag.name))?;

    let percent = match request.entity_id() {
        Some(entity_id) => hash_data(&flag.name, flag.hash_seed, entity_id)?,
        None => RANDOM.with(|rng| rng.borrow_mut().gen::<u16>())
    } % DISTRIBUTION_PERCENT_MAX;
    let converted_variant = get_converted_variant_for_percent(percent, &flag.variants, distributions);
    Ok(converted_variant)
}

fn hash_data(flag_name: &str, hash_seed: i64, entity_id: &str) -> Result<u16> {
    let data = format!("{}{}", flag_name, entity_id);
    let hash = xxh3_64_with_seed(data.as_bytes(), hash_seed as u64);
    Ok((hash % (u16::MAX as u64)) as u16)
}

fn match_request_and_get_distribution<'a, 'b>(
    request: &'a impl FlagRequest,
    flag: &'b Flag,
) -> Result<&'b Vec<Percent>> {
    for rule in &flag.rules {
        let satisfied = check_condition(request, &rule.condition)?;
        if satisfied {
            debug!(
                "Select rule '{}' for request {}",
                rule.name,
                DisplayFlagRequest(request)
            );
            return Ok(&rule.distributions);
        }
    }

    debug!(
        "No rule matched, use default distribution for request {}",
        DisplayFlagRequest(request)
    );
    Ok(&flag.default_distributions)
}

fn check_condition(request: &impl FlagRequest, condition: &Condition) -> Result<bool> {
    match condition {
        Condition::And(x, y) => Ok(check_condition(request, x)? && check_condition(request, y)?),
        Condition::Or(x, y) => Ok(check_condition(request, x)? || check_condition(request, y)?),
        Condition::IsEqualTo(parameter, condition_value) => {
            perform_check(request, parameter, condition_value, Check::IsEqualTo)
        }
        Condition::IsNotEqualTo(parameter, condition_value) => {
            perform_check(request, parameter, condition_value, Check::IsNotEqualTo)
        }
        Condition::GreaterThan(parameter, condition_value) => {
            perform_check(request, parameter, condition_value, Check::GreaterThan)
        }
        Condition::GreaterThanOrEqualTo(parameter, condition_value) => perform_check(
            request,
            parameter,
            condition_value,
            Check::GreaterThanOrEqualTo,
        ),
        Condition::LessThan(parameter, condition_value) => {
            perform_check(request, parameter, condition_value, Check::LessThan)
        }
        Condition::LessThanOrEqualTo(parameter, condition_value) => perform_check(
            request,
            parameter,
            condition_value,
            Check::LessThanOrEqualTo,
        ),
        Condition::In(parameter, condition_value) => {
            perform_check(request, parameter, condition_value, Check::In)
        }
        Condition::NotIn(parameter, condition_value) => {
            perform_check(request, parameter, condition_value, Check::NotIn)
        }
    }
}

fn perform_check(
    request: &impl FlagRequest,
    parameter: &Parameter,
    condition_value: &ConditionValue,
    check_type: Check,
) -> Result<bool> {
    match parameter {
        Parameter::EntityId =>
            match condition_value {
                ConditionValue::String(y) => Ok(checks::check(&request.entity_id(), &y, check_type)),
                ConditionValue::VecOfString(y) => Ok(checks::check_vec(&request.entity_id(), y, check_type)),
                x => Err(anyhow!(
                    "Parameter for entity_id is expected to have type 'string' or 'string array' but it's: '{}'",
                    type_of_condition_value(x)
                ))
            }

        Parameter::Context(name) => {
            match condition_value {
                ConditionValue::Bool(y) => request.context_value_bool(name).map(|x| checks::check(&x, y, check_type)),
                ConditionValue::String(y) => request.context_value_string(name).map(|x| checks::check(&x, &y, check_type)),
                ConditionValue::I64(y) => request.context_value_i64(name).map(|x| checks::check(&x, y, check_type)),
                ConditionValue::F64(y) => request.context_value_f64(name).map(|x| checks::check(&x, y, check_type)),
                ConditionValue::VecOfString(y) => request.context_value_string(name).map(|x| checks::check_vec(&x, y, check_type)),
                ConditionValue::VecOfI64(y) => request.context_value_i64(name).map(|x| checks::check_vec(&x.as_ref(), y, check_type)),
                ConditionValue::VecOfF64(y) => request.context_value_f64(name).map(|x| checks::check_vec(&x.as_ref(), y, check_type)),
            }
        }
    }
}

thread_local! {
    static RANDOM: RefCell<ThreadRng> = RefCell::new(rand::thread_rng());
}

fn get_converted_variant_for_percent(
    percent: u16,
    variants: &Variants,
    distributions: &[Percent],
) -> Option<Value> {
    let mut acc = 0;
    let mut has_enabled_value = false;
    for index in 0..distributions.len() {
        has_enabled_value = true;

        let Percent(distribution_percent) = distributions[index];
        acc += distribution_percent;

        if percent < acc {
            return Some(get_converted_variant(variants, index));
        }
    }

    if has_enabled_value {
        panic!(
            "Sum of percents of distributions must be 100%, actual is {}",
            acc as f32 / DISTRIBUTION_PERCENT_MAX as f32 / 100.0
        );
    }

    None
}

fn get_converted_variant(variants: &Variants, index: usize) -> Value {
    match variants {
        Variants::Bool(vec) => Value::Bool(vec[index].value),
        Variants::String(vec) => Value::String(vec[index].value.clone()),
        Variants::I64(vec) => Value::I64(vec[index].value),
        Variants::F64(vec) => Value::F64(vec[index].value),
    }
}

fn type_of_condition_value(condition_value: &ConditionValue) -> &str {
    use ConditionValue::*;

    match condition_value {
        Bool(_) => "bool",
        String(_) => "string",
        I64(_) => "integer",
        F64(_) => "float",
        VecOfString(_) => "StringArray",
        VecOfI64(_) => "IntegerArray",
        VecOfF64(_) => "FloatArray",
    }
}
