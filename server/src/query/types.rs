use anyhow::Result;
use common::flag::Flag;
use std::collections::HashMap;

pub type FlagsMap = HashMap<String, Flag>;

#[derive(Debug)]
pub enum Value {
    Bool(bool),
    String(String),
    I64(i64),
    F64(f64),
}

pub struct GetFlagResponse {
    pub value: Value,
}

pub struct GetFlagResult<'a> {
    pub name: &'a str,
    pub entity_id: Option<&'a str>,
    pub value: Value,
}

pub trait FlagRequest {
    fn flag_name(&self) -> &str;
    fn entity_id(&self) -> Option<&String>;
    fn context_value_bool(&self, key: &str) -> Result<Option<bool>>;
    fn context_value_string(&self, key: &str) -> Result<Option<&String>>;
    fn context_value_i64(&self, key: &str) -> Result<Option<i64>>;
    fn context_value_f64(&self, key: &str) -> Result<Option<f64>>;
}
