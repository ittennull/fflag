use super::api_dtos::{FlagRequestDto, FlagResponseDto, FlagsRequestDto, FlagsResponseDto, GetFlagResult};
use super::flag_validation;
use super::http_error::Error;
use crate::flag_storage::FlagStorage;
use crate::flags_synchronization::FlagsSynchronizer;
use crate::query::executor::Executor;
use crate::query::types::Value;
use anyhow::{anyhow, Result};
use axum::body::Bytes;
use axum::extract::{Path, State};
use axum::http::{header, HeaderMap, StatusCode};
use axum::response::{IntoResponse, Redirect};
use axum::routing::{delete, get, post, put};
use axum::{Json, Router};
use axum_otel_metrics::HttpMetricsLayerBuilder;
use common::flag::Flag;
use log::info;
use prost::bytes::Buf;
use serde::de::DeserializeOwned;
use std::net::SocketAddr;
use std::sync::Arc;
use tower_http::services::{ServeDir, ServeFile};

struct AppState {
    flag_storage: FlagStorage,
    flags_synchronizer: FlagsSynchronizer,
    executor: Arc<Executor>,
}

pub async fn start(
    address: SocketAddr,
    executor: Arc<Executor>,
    flag_storage: FlagStorage,
    flags_synchronizer: FlagsSynchronizer,
) -> Result<()> {
    let state = Arc::new(AppState {
        flag_storage,
        executor,
        flags_synchronizer,
    });

    let metrics = HttpMetricsLayerBuilder::new()
        .with_service_name("fflag".to_string())
        .with_service_version(env!("CARGO_PKG_VERSION").to_string())
        .build();

    let api_flags_router = Router::new()
        .route("/", get(get_flags))
        .route("/", post(create_flag))
        .route("/:flag_name", get(get_flag))
        .route("/:flag_name", delete(delete_flag))
        .route("/:flag_name", put(update_flag));

    let eval_router = Router::new()
        .route("/eval", post(eval))
        .route("/batch_eval", post(batch_eval));

    const UI_PREFIX: &str = "/fflag-ui";

    let app = Router::new()
        .merge(metrics.routes())
        .merge(eval_router)
        .nest("/api/flags", api_flags_router)
        .route("/", get(|| async { Redirect::permanent(UI_PREFIX) }))
        .nest_service(UI_PREFIX, ServeDir::new("ui/dist").fallback(ServeFile::new("ui/dist/index.html")))
        .with_state(state)
        .layer(metrics);
    let listener = tokio::net::TcpListener::bind(address).await?;
    info!("HTTP server is listening on {}", address);
    axum::serve(listener, app).await?;

    Ok(())
}

async fn eval(
    headers: HeaderMap,
    State(state): State<Arc<AppState>>,
    body: Bytes,
) -> Result<Json<FlagResponseDto>, Error> {
    let flag_request: FlagRequestDto = read_body(headers, body)?;

    let flag_response = state.executor.get_flag(&flag_request).await?;

    match flag_response {
        Some(result) => {
            let flag_response = FlagResponseDto {
                value: convert_from_domain_value(result.value),
            };
            Ok(Json(flag_response))
        }
        None => Err(Error::NotFound(anyhow!("Flag doesn't exist or disabled"))),
    }
}

async fn batch_eval(
    headers: HeaderMap,
    State(state): State<Arc<AppState>>,
    body: Bytes,
) -> Result<impl IntoResponse, Error> {
    let FlagsRequestDto(flags_requests) = read_body(headers, body)?;

    let responses = state.executor.get_flags(&flags_requests).await?;

    let flags_response = responses
        .into_iter()
        .map(|result| GetFlagResult {
            name: result.name,
            entity_id: result.entity_id,
            value: convert_from_domain_value(result.value),
        })
        .collect();
    let flags_response = FlagsResponseDto(flags_response);

    let json = serde_json::to_vec(&flags_response)?;

    let mut headers = HeaderMap::new();
    headers.insert(header::CONTENT_TYPE, "application/json".parse().unwrap());
    Ok((headers, json))
}

async fn get_flags(State(state): State<Arc<AppState>>) -> Result<Json<Vec<Flag>>, Error> {
    let flags = state.flag_storage.get_all_flags().await?;
    Ok(Json(flags))
}

async fn get_flag(
    Path(flag_name): Path<String>,
    State(state): State<Arc<AppState>>,
) -> Result<Json<Flag>, Error> {
    let flag = state
        .flag_storage
        .get_flag(&flag_name)
        .await?
        .ok_or(Error::NotFound(anyhow!("{} not found", flag_name)))?;
    Ok(Json(flag))
}

async fn create_flag(
    headers: HeaderMap,
    State(state): State<Arc<AppState>>,
    body: Bytes,
) -> Result<StatusCode, Error> {
    let flag = read_flag(headers, body)?;
    let name = flag.name.clone();
    state.flag_storage.save_flag(&name, flag).await?;
    state.flags_synchronizer.refresh().await?;
    Ok(StatusCode::CREATED)
}

async fn delete_flag(
    Path(flag_name): Path<String>,
    State(state): State<Arc<AppState>>,
) -> Result<(), Error> {
    state.flag_storage.delete(&flag_name).await?;
    state.flags_synchronizer.refresh().await?;
    Ok(())
}

async fn update_flag(
    Path(flag_name): Path<String>,
    headers: HeaderMap,
    State(state): State<Arc<AppState>>,
    body: Bytes,
) -> Result<(), Error> {
    let flag = read_flag(headers, body)?;
    state.flag_storage.save_flag(&flag_name, flag).await?;
    state.flags_synchronizer.refresh().await?;
    Ok(())
}

fn convert_from_domain_value(value: crate::query::types::Value) -> super::api_dtos::Value {
    use super::api_dtos::Value as Target;

    match value {
        Value::Bool(x) => Target::Bool(x),
        Value::String(x) => Target::String(x),
        Value::I64(x) => Target::Number(x as f64),
        Value::F64(x) => Target::Number(x),
    }
}

fn read_flag(headers: HeaderMap, body: Bytes) -> Result<Flag, Error> {
    let flag = read_body(headers, body)?;
    flag_validation::validate(&flag).map_err(Error::BadRequest)?;
    Ok(flag)
}

fn read_body<T: DeserializeOwned>(headers: HeaderMap, body: Bytes) -> Result<T, Error> {
    let is_json = headers
        .get(header::CONTENT_TYPE)
        .map_or(false, |x| x == "application/json");

    let data = match is_json {
        true => serde_json::from_reader(body.reader())?,
        false => serde_yaml::from_reader(body.reader())?,
    };
    Ok(data)
}
