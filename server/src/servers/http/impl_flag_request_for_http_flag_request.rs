use super::api_dtos::{FlagRequestDto, Value};
use crate::query::types::FlagRequest;
use anyhow::{anyhow, Error, Result};

const TYPE_BOOL: &str = "bool";
const TYPE_STRING: &str = "string";
const TYPE_NUMBER: &str = "number";

impl FlagRequest for FlagRequestDto {
    fn flag_name(&self) -> &str {
        &self.name
    }

    fn entity_id(&self) -> Option<&String> {
        self.entity_id.as_ref()
    }

    fn context_value_bool(&self, key: &str) -> Result<Option<bool>> {
        match self.context.get(key) {
            Some(Value::Bool(x)) => Ok(Some(*x)),
            Some(x) => Err(type_mismatch_error(key, TYPE_BOOL, x)),
            None => Ok(None),
        }
    }

    fn context_value_string(&self, key: &str) -> Result<Option<&String>> {
        match self.context.get(key) {
            Some(Value::String(x)) => Ok(Some(x)),
            Some(x) => Err(type_mismatch_error(key, TYPE_STRING, x)),
            None => Ok(None),
        }
    }

    fn context_value_i64(&self, key: &str) -> Result<Option<i64>> {
        match self.context.get(key) {
            Some(Value::Number(x)) => Ok(Some(*x as i64)),
            Some(x) => Err(type_mismatch_error(key, TYPE_NUMBER, x)),
            None => Ok(None),
        }
    }

    fn context_value_f64(&self, key: &str) -> Result<Option<f64>> {
        match self.context.get(key) {
            Some(Value::Number(x)) => Ok(Some(*x)),
            Some(x) => Err(type_mismatch_error(key, TYPE_NUMBER, x)),
            None => Ok(None),
        }
    }
}

fn type_mismatch_error(key: &str, expected: &str, x: &Value) -> Error {
    anyhow!(
        "Parameter '{}' is expected to be '{}' but it's '{}'",
        key,
        expected,
        get_type_name(x)
    )
}

fn get_type_name(value: &Value) -> &str {
    match value {
        Value::Bool(_) => TYPE_BOOL,
        Value::String(_) => TYPE_STRING,
        Value::Number(_) => TYPE_NUMBER,
    }
}
