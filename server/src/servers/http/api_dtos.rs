use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Deserialize)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct FlagRequestDto {
    pub name: String,
    pub entity_id: Option<String>,

    #[serde(default)]
    pub context: HashMap<String, Value>,
}

#[derive(Debug, Serialize)]
#[serde(rename_all(serialize = "camelCase"))]
pub struct FlagResponseDto {
    pub value: Value,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Value {
    Bool(bool),
    String(String),
    Number(f64),
}

#[derive(Debug, Deserialize)]
pub struct FlagsRequestDto(pub Vec<FlagRequestDto>);

#[derive(Debug, Serialize)]
pub struct FlagsResponseDto<'a>(pub Vec<GetFlagResult<'a>>);

#[derive(Debug, Serialize)]
pub struct GetFlagResult<'a> {
    pub name: &'a str,
    pub entity_id: Option<&'a str>,
    pub value: Value,
}
