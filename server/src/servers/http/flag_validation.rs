use anyhow::{bail, Result};
use common::flag::{Condition, ConditionValue, Flag, Parameter, Percent, Rule, Variant, Variants, DISTRIBUTION_PERCENT_MAX};
use itertools::Itertools;
use std::collections::HashMap;
use std::mem;

type ErrorStr = &'static str;

pub fn validate(flag: &Flag) -> Result<()> {
    // check '~' because of a bug in serde_yaml https://github.com/dtolnay/serde-yaml/issues/87
    if flag.name.trim().is_empty() || flag.name == "~" {
        bail!("Flag name cannot be empty");
    }

    if !flag
        .name
        .chars()
        .all(|x| x.is_ascii_alphanumeric() || x == '_' || x == '-')
    {
        bail!("Flag name must be alphanumeric ASCII and can contain '-', '_'");
    }

    if let Some(error) = validate_variants(&flag.variants) {
        bail!(error);
    }

    if let Some(error) = validate_distribution(&flag.default_distributions) {
        bail!("Default distribution: {}", error);
    }

    for rule in &flag.rules {
        if let Some(error) = validate_condition(&rule.condition) {
            bail!("Rule '{}': {}", rule.name, error);
        }

        if let Some(error) = validate_distribution(&rule.distributions) {
            bail!("Rule '{}': {}", rule.name, error);
        }
    }

    if let Some(error) = validate_same_parameter_same_type(&flag.rules) {
        bail!(error);
    }

    Ok(())
}

fn validate_variants(variants: &Variants) -> Option<ErrorStr> {
    fn check<T: PartialEq>(vec: &[Variant<T>]) -> Option<ErrorStr> {
        if vec.len() < 2 {
            return Some("There must be at least 2 possible values");
        }

        if vec.iter().any(|x| x.name.is_empty()) {
            return Some("Variant name cannot be empty");
        }

        let unique_variant_names = vec.iter().unique_by(|x| &x.name).count();
        if unique_variant_names != vec.len() {
            return Some("Variant names must be unique");
        }

        // find duplicates manually using PartialEq because f64 is not `Eq` and unique_by can't be used
        for i in 1..vec.len() {
            for j in 0..i {
                if vec[i].value.eq(&vec[j].value) {
                    return Some("Variant values must be unique");
                }
            }
        }

        None
    }

    match variants {
        Variants::Bool(vec) => check(vec),
        Variants::String(vec) => check(vec),
        Variants::I64(vec) => check(vec),
        Variants::F64(vec) => check(vec),
    }
}

fn validate_distribution(distributions: &[Percent]) -> Option<ErrorStr> {
    let sum: u16 = distributions
        .iter()
        .map(|x| x.0)
        .sum();

    match sum {
        DISTRIBUTION_PERCENT_MAX => None,
        _ => Some("Sum of all distribution values must be 100%"),
    }
}

fn validate_condition(condition: &Condition) -> Option<ErrorStr> {
    visit_condition(condition, &mut |parameter: &Parameter, _| match parameter {
        Parameter::Context(x) if x.is_empty() => {
            Some("Parameter name in condition cannot be empty")
        }
        _ => None,
    })
}

fn validate_same_parameter_same_type(rules: &[Rule]) -> Option<String> {
    let mut types = HashMap::new();

    rules.iter().find_map(|x| {
        visit_condition(
            &x.condition,
            &mut |parameter, condition_value| match parameter {
                Parameter::Context(name) => {
                    match types.insert(name, condition_value) {
                        Some(old_value) if mem::discriminant(old_value) != mem::discriminant(condition_value) =>
                            Some(format!(
                                "Parameter '{}' has different types in different rules or within the same rule",
                                name
                            )),
                        _ => None
                    }
                }
                _ => None,
            },
        )
    })
}

fn visit_condition<'a, T>(
    condition: &'a Condition,
    f: &mut impl FnMut(&'a Parameter, &'a ConditionValue) -> Option<T>,
) -> Option<T> {
    match condition {
        Condition::And(left, right) | Condition::Or(left, right) => {
            visit_condition(left, f).or_else(|| visit_condition(right, f))
        }
        Condition::IsEqualTo(parameter, value)
        | Condition::IsNotEqualTo(parameter, value)
        | Condition::GreaterThan(parameter, value)
        | Condition::GreaterThanOrEqualTo(parameter, value)
        | Condition::LessThan(parameter, value)
        | Condition::LessThanOrEqualTo(parameter, value)
        | Condition::In(parameter, value)
        | Condition::NotIn(parameter, value) => f(parameter, value),
    }
}
