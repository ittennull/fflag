use crate::group_version::GroupVersions;
use crate::query::types::FlagsMap;
use common::flag::{Flag, Percent};
use flag_configuration_contracts::condition::{ParameterAndConditionValue, TwoConditions};
use flag_configuration_contracts::variants::{BoolVariants, F64Variants, I64Variants, StringVariants};
use flag_configuration_contracts::{Condition, FlagConfiguration, GetNewConfigurationRequest, GetNewConfigurationResponse, Group, Rule, Variants};
use flag_configuration_contracts::{ConditionValue, Parameter};
use std::collections::HashSet;
use std::sync::Arc;
use tokio::sync::RwLock;
use tonic::{Request, Response, Status};

pub mod flag_configuration_contracts {
    tonic::include_proto!("flag_configuration");
}

pub struct FlagConfigurationService {
    pub flags: Arc<RwLock<FlagsMap>>,
    pub groups: Arc<RwLock<GroupVersions>>,
}

#[tonic::async_trait]
impl flag_configuration_contracts::flag_configuration_service_server::FlagConfigurationService for FlagConfigurationService {
    async fn get_new_configuration(&self, request: Request<GetNewConfigurationRequest>) -> Result<Response<GetNewConfigurationResponse>, Status> {
        let updated_groups = find_updated_group_names(Arc::clone(&self.groups), request.into_inner().known_groups).await;
        let flag_configurations = get_flag_configurations_for_groups(Arc::clone(&self.flags), &updated_groups).await;

        Ok(Response::new(GetNewConfigurationResponse {
            groups: updated_groups,
            flag_configurations,
        }))
    }
}

async fn find_updated_group_names(groups: Arc<RwLock<GroupVersions>>, known_groups: Vec<Group>) -> Vec<Group> {
    let group_versions = groups.read().await;

    known_groups.into_iter()
        .filter_map(|known_group|
            group_versions.get(&known_group.name)
                .filter(|group_version| known_group.version < group_version.version)
                .map(|group_version| Group {
                    name: known_group.name,
                    version: group_version.version,
                })
        )
        .collect()
}

async fn get_flag_configurations_for_groups(flags: Arc<RwLock<FlagsMap>>, groups: &[Group]) -> Vec<FlagConfiguration> {
    let flags = flags.read().await;

    let mut flag_configurations = Vec::new();
    let mut set = HashSet::new();
    for group in groups {
        for (flag_name, flag) in flags.iter() {
            if flag.groups.contains(&group.name) && set.insert(flag_name) {
                flag_configurations.push(convert_flag(flag));
            }
        }
    }

    flag_configurations
}

fn convert_flag(flag: &Flag) -> FlagConfiguration {
    FlagConfiguration {
        name: flag.name.clone(),
        enabled: flag.enabled,
        variants: convert_variants(&flag.variants),
        default_distributions: convert_distributions(&flag.default_distributions),
        rules: convert_rules(&flag.rules),
        hash_seed: flag.hash_seed,
        groups: flag.groups.clone(),
    }
}

fn convert_variants(variants: &common::flag::Variants) -> Option<Variants> {
    use common::flag::Variants::*;
    use flag_configuration_contracts::variants::Variants as Case;

    let case = match variants {
        Bool(vec) => Case::Bools(BoolVariants { values: vec.iter().map(|x| x.value).collect() }),
        String(vec) => Case::Strings(StringVariants { values: vec.iter().map(|x| x.value.clone()).collect() }),
        I64(vec) => Case::I64s(I64Variants { values: vec.iter().map(|x| x.value).collect() }),
        F64(vec) => Case::F64s(F64Variants { values: vec.iter().map(|x| x.value).collect() }),
    };

    Some(Variants {
        variants: Some(case)
    })
}

fn convert_distributions(distributions: &[Percent]) -> Vec<u32> {
    distributions.iter().map(|x| x.0 as u32).collect()
}

fn convert_rules(rules: &[common::flag::Rule]) -> Vec<Rule> {
    rules.iter().map(|rule| Rule {
        name: rule.name.clone(),
        distributions: convert_distributions(&rule.distributions),
        condition: Some(convert_condition(&rule.condition)),
    }).collect()
}

fn convert_condition(condition: &common::flag::Condition) -> Condition {
    use common::flag::Condition::*;
    use flag_configuration_contracts::condition::Conditions as Case;

    let case = match condition {
        And(left, right) => Case::And(Box::new(TwoConditions {
            left: Some(Box::new(convert_condition(left))),
            right: Some(Box::new(convert_condition(right))),
        })),
        Or(left, right) => Case::Or(Box::new(TwoConditions {
            left: Some(Box::new(convert_condition(left))),
            right: Some(Box::new(convert_condition(right))),
        })),
        IsEqualTo(param, value) => Case::IsEqualTo(convert_condition_op(param, value)),
        IsNotEqualTo(param, value) => Case::IsNotEqualTo(convert_condition_op(param, value)),
        GreaterThan(param, value) => Case::GreaterThan(convert_condition_op(param, value)),
        GreaterThanOrEqualTo(param, value) => Case::GreaterThanOrEqualTo(convert_condition_op(param, value)),
        LessThan(param, value) => Case::LessThan(convert_condition_op(param, value)),
        LessThanOrEqualTo(param, value) => Case::LessThanOrEqualTo(convert_condition_op(param, value)),
        In(param, value) => Case::In(convert_condition_op(param, value)),
        NotIn(param, value) => Case::NotIn(convert_condition_op(param, value)),
    };

    Condition {
        conditions: Some(case)
    }
}

fn convert_condition_op(param: &common::flag::Parameter, value: &common::flag::ConditionValue) -> ParameterAndConditionValue {
    ParameterAndConditionValue {
        parameter: Some(Parameter {
            is_entity_id: matches!(param, common::flag::Parameter::EntityId),
            context_value: match param {
                common::flag::Parameter::Context(x) => Some(x.clone()),
                _ => None
            },
        }),
        condition_value: convert_condition_value(value),
    }
}

fn convert_condition_value(value: &common::flag::ConditionValue) -> Option<ConditionValue> {
    use common::flag::ConditionValue::*;
    use flag_configuration_contracts::condition_value as grpc;
    use grpc::ConditionValues as Case;

    let case = match value {
        Bool(x) => Case::Bool(*x),
        String(x) => Case::String(x.clone()),
        I64(x) => Case::I64(*x),
        F64(x) => Case::F64(*x),
        VecOfString(x) => Case::Strings(grpc::VecOfString { values: x.clone() }),
        VecOfI64(x) => Case::I64s(grpc::VecOfI64 { values: x.clone() }),
        VecOfF64(x) => Case::F64s(grpc::VecOfF64 { values: x.clone() }),
    };

    Some(ConditionValue {
        condition_values: Some(case)
    })
}