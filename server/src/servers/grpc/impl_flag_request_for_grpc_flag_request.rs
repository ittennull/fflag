use crate::query::types::FlagRequest;
use crate::servers::grpc::query_service::query_contracts::value::OneOfValue;
use crate::servers::grpc::query_service::query_contracts::Value;
use anyhow::{anyhow, Error, Result};
use std::fmt;

impl FlagRequest for super::query_service::query_contracts::FlagRequest {
    fn flag_name(&self) -> &str {
        &self.name
    }

    fn entity_id(&self) -> Option<&String> {
        if self.entity_id.is_empty() {
            None
        } else {
            Some(&self.entity_id)
        }
    }

    fn context_value_bool(&self, key: &str) -> Result<Option<bool>> {
        match self.context.get(key) {
            Some(Value {
                     one_of_value: Some(OneOfValue::Bool(x)),
                 }) => Ok(Some(*x)),
            Some(Value {
                     one_of_value: Some(x),
                 }) => Err(type_mismatch_error(key, "bool", x)),
            _ => Ok(None),
        }
    }

    fn context_value_string(&self, key: &str) -> Result<Option<&String>> {
        match self.context.get(key) {
            Some(Value {
                     one_of_value: Some(OneOfValue::String(x)),
                 }) => Ok(Some(x)),
            Some(Value {
                     one_of_value: Some(x),
                 }) => Err(type_mismatch_error(key, "string", x)),
            _ => Ok(None),
        }
    }

    fn context_value_i64(&self, key: &str) -> Result<Option<i64>> {
        match self.context.get(key) {
            Some(Value {
                     one_of_value: Some(OneOfValue::Int(x)),
                 }) => Ok(Some(*x)),
            Some(Value {
                     one_of_value: Some(x),
                 }) => Err(type_mismatch_error(key, "long", x)),
            _ => Ok(None),
        }
    }

    fn context_value_f64(&self, key: &str) -> Result<Option<f64>> {
        match self.context.get(key) {
            Some(Value {
                     one_of_value: Some(OneOfValue::Double(x)),
                 }) => Ok(Some(*x)),
            Some(Value {
                     one_of_value: Some(x),
                 }) => Err(type_mismatch_error(key, "double", x)),
            _ => Ok(None),
        }
    }
}

fn type_mismatch_error(key: &str, expected: &str, value: &OneOfValue) -> Error {
    anyhow!(
        "Parameter '{}' is expected to be '{}' but it's '{}'",
        key,
        expected,
        value
    )
}

impl fmt::Display for OneOfValue {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use OneOfValue::*;

        match self {
            Bool(_) => write!(f, "bool"),
            String(_) => write!(f, "string"),
            Int(_) => write!(f, "long"),
            Double(_) => write!(f, "double"),
        }
    }
}
