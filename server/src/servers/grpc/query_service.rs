use crate::query::executor::Executor;
use crate::query::types::Value;
use query_contracts::{FlagRequest, FlagResponse, FlagsRequest, FlagsResponse, GetFlagResult};
use std::sync::Arc;
use tonic::{Request, Response, Status};

pub mod query_contracts {
    tonic::include_proto!("flags");
}

pub struct QueryService {
    pub executor: Arc<Executor>,
}

#[tonic::async_trait]
impl query_contracts::query_server::Query for QueryService {
    async fn get_flag(
        &self,
        request: Request<FlagRequest>,
    ) -> Result<Response<FlagResponse>, Status> {
        let message = request.get_ref();

        match self.executor.get_flag(message).await {
            Ok(Some(response)) => Ok(Response::new(FlagResponse {
                value: to_grpc_value(response.value),
            })),
            Ok(None) => Err(Status::not_found(format!("{} not found", message.name))),
            Err(err) => Err(get_error_response(&err)),
        }
    }

    async fn get_flags(
        &self,
        request: Request<FlagsRequest>,
    ) -> Result<Response<FlagsResponse>, Status> {
        let message = request.get_ref();

        match self.executor.get_flags(&message.requests).await {
            Ok(results) => {
                let flags_response = FlagsResponse {
                    results: results
                        .into_iter()
                        .map(|result| GetFlagResult {
                            name: result.name.to_owned(),
                            entity_id: result.entity_id.map_or(String::from(""), |x| x.to_owned()),
                            value: to_grpc_value(result.value),
                        })
                        .collect(),
                };

                Ok(Response::new(flags_response))
            }
            Err(err) => Err(get_error_response(&err)),
        }
    }
}

fn to_grpc_value(value: Value) -> Option<query_contracts::Value> {
    Some(query_contracts::Value {
        one_of_value: Some(convert_from_domain_value(value)),
    })
}

fn get_error_response(err: &anyhow::Error) -> Status {
    Status::invalid_argument(format!("Something went wrong: {}", err))
}

fn convert_from_domain_value(value: Value) -> query_contracts::value::OneOfValue {
    use query_contracts::value::OneOfValue as Target;

    match value {
        Value::Bool(x) => Target::Bool(x),
        Value::String(x) => Target::String(x),
        Value::I64(x) => Target::Int(x),
        Value::F64(x) => Target::Double(x),
    }
}
