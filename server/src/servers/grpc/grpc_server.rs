use crate::group_version::GroupVersions;
use crate::query::executor::Executor;
use crate::query::types::FlagsMap;
use crate::servers::grpc::{
    flag_configuration_service::flag_configuration_contracts::flag_configuration_service_server::FlagConfigurationServiceServer,
    flag_configuration_service::FlagConfigurationService,
    query_service::query_contracts::query_server::QueryServer,
    query_service::QueryService,
};
use anyhow::{anyhow, Result};
use log::info;
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::sync::RwLock;

pub async fn start(address: SocketAddr,
                   executor: Arc<Executor>,
                   flags: Arc<RwLock<FlagsMap>>,
                   groups: Arc<RwLock<GroupVersions>>) -> Result<()> {
    let query_service = QueryService { executor };
    let flag_configuration_service = FlagConfigurationService { flags, groups };

    info!("GRPC server is listening on {}", address);

    tonic::transport::Server::builder()
        .add_service(QueryServer::new(query_service))
        .add_service(FlagConfigurationServiceServer::new(flag_configuration_service))
        .serve(address)
        .await
        .map_err(|x| anyhow!(x))
}

