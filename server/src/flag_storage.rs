use crate::group_version::GroupVersion;
use anyhow::{anyhow, Context, Result};
use common::flag::Flag;
use futures::{StreamExt, TryStreamExt};
use mongodb::bson::{doc, Document};
use mongodb::options::{UpdateOneModel, WriteModel};
use mongodb::{bson, Client, Collection};
use nameof::name_of;
use std::collections::HashSet;

const DATABASE_NAME: &str = "fflag";

#[derive(Clone)]
pub struct FlagStorage {
    client: Client,
}

impl FlagStorage {
    pub async fn new(connection_string: &str) -> Result<Self> {
        let client = Client::with_uri_str(connection_string).await.context(
            format!("Creating a mongodb client with connection string '{}'", connection_string))?;

        Ok(Self { client })
    }

    pub async fn get_all_flags(&self) -> Result<Vec<Flag>> {
        let cursor = self.collection().find(Document::default()).await?;
        let flags: Vec<_> = cursor.try_collect().await?;
        Ok(flags)
    }

    pub async fn get_flag(&self, name: &str) -> Result<Option<Flag>> {
        let filter = id_filter(name);
        let mut cursor = self.collection().find(filter).await?;
        if let Some(result) = cursor.next().await {
            return result
                .map(Some)
                .map_err(|e| anyhow!("Mongodb error: {}", e));
        }

        Ok(None)
    }

    pub async fn save_flag(&self, name: &str, flag: Flag) -> Result<()> {
        let mut groups = HashSet::with_capacity(flag.groups.len());
        groups.extend(flag.groups.clone());

        let existing = self.get_flag(name).await?;
        if let Some(existing_flag) = existing {
            groups.extend(existing_flag.groups);

            if existing_flag.name != flag.name {
                let filter = id_filter(&existing_flag.name);
                self.collection().delete_one(filter).await?;
            }
        }

        // collect write models for the updating of the flag and all its group versions
        let mut write_models = Vec::with_capacity(groups.len() + 1);

        // update group versions
        self.create_update_version_models(&mut write_models, groups);

        // update flag
        let now = chrono::Utc::now().to_rfc3339();
        write_models.push(
            WriteModel::UpdateOne(
                UpdateOneModel::builder()
                    .namespace(self.collection().namespace())
                    .filter(id_filter(&flag.name))
                    .update(doc! {
                        "$set": doc!{
                            name_of!(enabled in Flag): flag.enabled,
                            name_of!(variants in Flag): bson::to_bson(&flag.variants)?,
                            name_of!(default_distributions in Flag): bson::to_bson(&flag.default_distributions)?,
                            name_of!(rules in Flag): bson::to_bson(&flag.rules)?,
                            name_of!(hash_seed in Flag): flag.hash_seed,
                            name_of!(groups in Flag): flag.groups.iter().filter(|x| !x.is_empty()).collect::<Vec<_>>(),
                            name_of!(updated_at in Flag): &now
                        },
                        "$setOnInsert": doc!{ name_of!(created_at in Flag): now }
                    })
                    .upsert(true)
                    .build(),
            ));

        self.client.bulk_write(write_models).await?;
        Ok(())
    }

    pub async fn delete(&self, flag_name: &str) -> Result<()> {
        // find the flag and delete it
        let flag = self.find_and_delete_flag(flag_name).await?;

        // update group versions
        if let Some(flag) = flag {
            if !flag.groups.is_empty() {
                let mut write_models: Vec<WriteModel> = Vec::with_capacity(flag.groups.len());
                self.create_update_version_models(&mut write_models, flag.groups);
                self.client.bulk_write(write_models).await?;
            }
        }

        Ok(())
    }

    pub async fn get_all_groups(&self) -> Result<Vec<GroupVersion>> {
        let cursor = self.group_versions_collection().find(Document::default()).await?;
        let groups = cursor.try_collect().await?;
        Ok(groups)
    }

    async fn find_and_delete_flag(&self, flag_name: &str) -> Result<Option<Flag>> {
        let filter = id_filter(flag_name);
        let flag = self.collection().find_one_and_delete(filter).await?;
        Ok(flag)
    }

    fn create_update_version_models(&self, write_models: &mut Vec<WriteModel>, groups: impl IntoIterator<Item=String>) {
        for group in groups {
            write_models.push(
                WriteModel::UpdateOne(
                    UpdateOneModel::builder()
                        .namespace(self.group_versions_collection().namespace())
                        .filter(id_filter(&group))
                        .update(doc! {
                            "$inc": doc!{
                                name_of!(version in GroupVersion): 1
                            }
                        })
                        .upsert(true)
                        .build(),
                ));
        }
    }

    fn collection(&self) -> Collection<Flag> {
        self.client.database(DATABASE_NAME).collection("flags")
    }

    fn group_versions_collection(&self) -> Collection<GroupVersion> {
        self.client.database(DATABASE_NAME).collection("group_versions")
    }
}

fn id_filter(value: &str) -> Document {
    doc! { "_id": value }
}