use serde::Deserialize;
use std::collections::HashMap;

#[derive(Deserialize)]
pub struct GroupVersion {
    #[serde(rename = "_id")]
    pub group_name: String,
    pub version: i64,
}

pub type GroupVersions = HashMap<String, GroupVersion>;