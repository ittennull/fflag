use serde::Deserialize;
use std::net::SocketAddr;

#[derive(Deserialize)]
pub struct AppConfig {
    pub mongodb_connection_string: String,
    pub server: Server,
    pub sync_interval: u64,
    pub json_logs: bool,
}

#[derive(Deserialize)]
pub struct Server {
    pub grpc_address: SocketAddr,
    pub http_address: SocketAddr,
}
