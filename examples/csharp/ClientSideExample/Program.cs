using FflagClient;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddFflagClient(options =>
{
    options.ServerUri = new Uri("http://localhost:9090");
    options.UpdateIntervalInSeconds = 20;
    options.WatchedGroups.Add("this-app-name");
});


var app = builder.Build();

app.MapGet("/", (IFlagEvaluator flagEvaluator, string flagName, string? userId, string? countryCode, int? age) =>
{
    return flagEvaluator.GetBool(flagName, userId, Parameters.Add(countryCode).Add(age));
});

app.Run();
