1. Run the fflag service
2. Import a flag using the "Import" button:
```yaml
name: useNewColorTheme
enabled: true
variants: !Bool
- name: Not yet
  value: false
- name: Yes
  value: true
default_distributions:
- 500
- 500
rules:
- name: Yes for a list of countries or if above 20 years old
  distributions:
  - 0
  - 1000
  condition: !Or
  - !In
    - !Context countryCode
    - !VecOfString
      - NL
      - DE
      - FR
      - BE
  - !GreaterThan
    - !Context age
    - !I64 20
hash_seed: 3080083593610417984
groups:
- this-app-name
```
3. Run the ExampleApp
4. Use Requests.http to run the example requests