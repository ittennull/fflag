# .net8 grpc client

The example creates a boolean flag with a name _UseNewShoppingCart_ and queries it using single flag request `GetFlag`
and batch request `GetFlags`.

This is an example of server-side flag evaluation. The client side is the preferred one, but it's
implemented only for C#. All other languages can implement it too or use this server-side evaluation

To run the example:

- start FFlag (see [documentation](https://ittennull.gitlab.io/fflag/installation.html))
- [install .net](https://dotnet.microsoft.com/download) if you don't have it.
- execute: `dotnet run`