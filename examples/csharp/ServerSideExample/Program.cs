using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Flags;
using Grpc.Net.Client;
using static Flags.Query;

await CreateFlag("http://localhost:8080");

var client = new QueryClient(GrpcChannel.ForAddress(new Uri("http://localhost:9090")));
await GetSingleFlag(client);
await GetMultipleFlags(client);


static async Task GetSingleFlag(QueryClient client)
{
    var response = await client.GetFlagAsync(new FlagRequest
    {
        Name = "UseNewShoppingCart"
    });

    Console.WriteLine("Get UseNewShoppingCart flag returned: {0}", response.Value);
}

static async Task GetMultipleFlags(QueryClient client)
{
    var response = await client.GetFlagsAsync(new FlagsRequest
    {
        Requests =
        {
            new FlagRequest
            {
                Name = "UseNewShoppingCart",
                EntityId = "Alice",
                Context = {["age"] = new Value {Int = 20}}
            },
            new FlagRequest
            {
                Name = "UseNewShoppingCart",
                EntityId = "Bob",
                Context = {["age"] = new Value {Int = 40}}
            }
        }
    });

    Console.WriteLine("Get UseNewShoppingCart flag for two users returned:");
    Console.WriteLine("•Alice: {0}", response.Results.Single(x => x.EntityId == "Alice").Value.Bool);
    Console.WriteLine("•Bob: {0}", response.Results.Single(x => x.EntityId == "Bob").Value.Bool);
}

static async Task CreateFlag(string baseAddress)
{
    var flagSource = @"
name: UseNewShoppingCart
enabled: true
variants: !Bool
- name: Old
  value: false
- name: New
  value: true
default_distributions:
- 500
- 500
rules:
- name: Users older than 30 get new shopping cart
  distributions:
  - 0
  - 1000
  condition: !GreaterThan
  - !Context age
  - !I64 30
hash_seed: 123456789
groups: []
";
    using var httpClient = new HttpClient();
    using var response = await httpClient.PostAsync($"{baseAddress}/api/flags", new StringContent(flagSource));
    response.EnsureSuccessStatusCode();
    
    Console.WriteLine("Created flag UseNewShoppingCart");
}
