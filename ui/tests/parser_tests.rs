use common::flag::Condition::*;
use common::flag::ConditionValue::*;
use common::flag::Parameter::*;
use ui::condition_parser::parser::parse;
use ui::condition_parser::types::{ParsingError, ParsingErrorType};

#[test]
fn property_name_with_underscore_equals_string() {
    let result = parse(r#"first_name == "John""#);
    let expected = Ok(IsEqualTo(
        Context("first_name".into()),
        String("John".into()),
    ));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_string() {
    let result = parse(r#"name1 == "other""#);
    let expected = Ok(IsEqualTo(Context("name1".into()), String("other".into())));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_empty_string() {
    let result = parse(r#"name1 == """#);
    let expected = Ok(IsEqualTo(Context("name1".into()), String("".into())));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_false() {
    let result = parse(r#"name1 == false"#);
    let expected = Ok(IsEqualTo(Context("name1".into()), Bool(false)));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_true() {
    let result = parse(r#"name1 == true"#);
    let expected = Ok(IsEqualTo(Context("name1".into()), Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_integer() {
    let result = parse(r#"name1 == 123"#);
    let expected = Ok(IsEqualTo(Context("name1".into()), I64(123)));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_negative_integer() {
    let result = parse(r#"name1 == -123"#);
    let expected = Ok(IsEqualTo(Context("name1".into()), I64(-123)));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_float() {
    let result = parse(r#"name1 == 123.4"#);
    let expected = Ok(IsEqualTo(Context("name1".into()), F64(123.4)));
    assert_eq!(result, expected);
}

#[test]
fn property_name_equals_negative_float() {
    let result = parse(r#"name1 == -123.4"#);
    let expected = Ok(IsEqualTo(Context("name1".into()), F64(-123.4)));
    assert_eq!(result, expected);
}

#[test]
fn entity_id_equals_integer() {
    let result = parse(r#"$EntityId == 12"#);
    let expected = Ok(IsEqualTo(EntityId, I64(12)));
    assert_eq!(result, expected);
}

#[test]
fn entity_id_in_integer_array() {
    let result = parse(r#"$EntityId In [ 1,23, 456]"#);
    let expected = Ok(In(EntityId, VecOfI64(vec![1, 23, 456])));
    assert_eq!(result, expected);
}

#[test]
fn entity_id_notin_float_array() {
    let result = parse(r#"$EntityId NotIn [ 1.0,23.56, 456.789]"#);
    let expected = Ok(NotIn(EntityId, VecOfF64(vec![1.0, 23.56, 456.789])));
    assert_eq!(result, expected);
}

#[test]
fn entity_id_in_string_array() {
    let result = parse(r#"$EntityId In ["a", "", "ab,c def,"]"#);
    let expected = Ok(In(
        EntityId,
        VecOfString(vec!["a".into(), "".into(), "ab,c def,".into()]),
    ));
    assert_eq!(result, expected);
}

#[test]
fn equals_and_less_than() {
    let result = parse(r#"Param1 == "val 1" AND Param2 < 45  "#);
    let expected = Ok(And(
        Box::new(IsEqualTo(Context("Param1".into()), String("val 1".into()))),
        Box::new(LessThan(Context("Param2".into()), I64(45))),
    ));
    assert_eq!(result, expected);
}

#[test]
fn several_conditions() {
    let result = parse(r#"Param1 == "val 1" AND Param2 < 45 OR $EntityId == false"#);
    let expected = Ok(And(
        Box::new(IsEqualTo(Context("Param1".into()), String("val 1".into()))),
        Box::new(Or(
            Box::new(LessThan(Context("Param2".into()), I64(45))),
            Box::new(IsEqualTo(EntityId, Bool(false))),
        )),
    ));
    assert_eq!(result, expected);
}

#[test]
fn one_condition_group() {
    let result = parse(r#"($EntityId == true)"#);
    let expected = Ok(IsEqualTo(EntityId, Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn one_condition_nested_groups() {
    let result = parse(r#"(($EntityId == true))"#);
    let expected = Ok(IsEqualTo(EntityId, Bool(true)));
    assert_eq!(result, expected);
}

#[test]
fn or_group_and_condition() {
    let result = parse(r#"(Param1 == "val 1" OR Param3 <= 3) AND Param2 < 45"#);
    let expected = Ok(And(
        Box::new(Or(
            Box::new(IsEqualTo(Context("Param1".into()), String("val 1".into()))),
            Box::new(LessThanOrEqualTo(Context("Param3".into()), I64(3))),
        )),
        Box::new(LessThan(Context("Param2".into()), I64(45))),
    ));
    assert_eq!(result, expected);
}

#[test]
fn or_group1_or_or_group2() {
    let result =
        parse(r#"(Param1 == "val 1" OR Param3 <= 3) OR ((Param2 < 45) OR ($EntityId >= -20.123))"#);
    let expected = Ok(Or(
        Box::new(Or(
            Box::new(IsEqualTo(Context("Param1".into()), String("val 1".into()))),
            Box::new(LessThanOrEqualTo(Context("Param3".into()), I64(3))),
        )),
        Box::new(Or(
            Box::new(LessThan(Context("Param2".into()), I64(45))),
            Box::new(GreaterThanOrEqualTo(EntityId, F64(-20.123))),
        )),
    ));
    assert_eq!(result, expected);
}

#[test]
fn mismatched_parenthesis1() {
    let result = parse(r#"(first_name == "John""#);
    assert_eq!(
        result,
        Err(ParsingError::new(
            ParsingErrorType::ParenthesisMismatch,
            None
        ))
    );
}

#[test]
fn mismatched_parenthesis2() {
    let result = parse(r#"first_name == "John")"#);
    assert_eq!(
        result,
        Err(ParsingError::new(
            ParsingErrorType::ParenthesisMismatch,
            None
        ))
    );
}

#[test]
fn bad_value1() {
    let result = parse(r#"first_name == John"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectOperandValue,
            ..
        })
    ));
}

#[test]
fn empty_array_is_not_allowed() {
    let result = parse(r#"first_name == []"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectOperandValue,
            ..
        })
    ));
}

#[test]
fn array_of_different_types_is_not_allowed() {
    let result = parse(r#"first_name == [1, "a"]"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectOperandValue,
            ..
        })
    ));
}

#[test]
fn operator_requires_array_operand1() {
    let result = parse(r#"param In 2"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectArray,
            ..
        })
    ));
}

#[test]
fn operator_requires_array_operand2() {
    let result = parse(r#"param NotIn true"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectArray,
            ..
        })
    ));
}

#[test]
fn operator_requires_scalar_operand1() {
    let result = parse(r#"param < [1,2,3]"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectScalar,
            ..
        })
    ));
}

#[test]
fn operator_requires_scalar_operand2() {
    let result = parse(r#"param == [1,2,3]"#);
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::ExpectScalar,
            ..
        })
    ));
}

#[test]
fn empty_input() {
    let result = parse("");
    assert!(matches!(
        result,
        Err(ParsingError {
            error_type: ParsingErrorType::EmptyInput,
            ..
        })
    ));
}
