# Development

1. `npm install`
2. `cargo install dioxus-cli`
3. Run the following command in the root of the project to start the tailwind CSS compiler:

```bash
npx tailwindcss -i ./input.css -o ./assets/tailwind.css --watch
```

Dioxus dev server can be also used to have auto-reload feature (but some changes in Dioxus.toml are needed, like proxy
server):

```bash
dx serve --hot-reload
```
