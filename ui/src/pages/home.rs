use dioxus::prelude::*;
use crate::components::import_dialog;
use crate::components::import_dialog::ImportDialog;
use crate::Route;
use crate::server_api::ServerApi;

#[component]
pub fn Home() -> Element {
    let mut flags = use_signal(Vec::new);
    let server_api = use_context::<ServerApi>();

    let mut load_flags = {
        let server_api = server_api.clone();
        use_resource(move || {
            let server_api = server_api.clone();
            async move {
                if let Ok(data) = server_api.get_flags().await{
                    flags.set(data);
                }
            }
        })
    };

    rsx! {
        div{
            class: "flex gap-5",
            button{
                class: "btn btn-primary",
                Link{
                    to: Route::NewFlag {},
                    "New flag"
                }
            }
            button{
                class: "btn btn-outline",
                onclick: move |_| {
                    eval(&format!("document.getElementById('{}').showModal();", import_dialog::DIALOG_ID));
                },
                "Import"
            }
        }

        ImportDialog{
            on_import: {
                to_owned![server_api];
                move |yaml|{
                    to_owned![server_api];
                    spawn(async move {
                        if let Ok(()) = server_api.import_flag(yaml).await{
                            load_flags.restart();
                        }
                    });
                }
            }
        }

        table {
            class: "table mt-3 text-lg",
            thead {
                class: "text-lg",
                tr {
                    th { "Name" }
                    th {
                        class: "w-1",
                        "Created at"
                    }
                    th {
                        class: "w-1",
                        "Updated at"
                    }
                    th {
                        class: "w-1 text-right",
                        "Status"
                    }
                }
            }
            tbody {
                for flag in flags.iter(){
                    tr {
                        key: "{flag.name}",
                        class: "hover",
                        td {
                            Link{
                                to: Route::Flag { name: flag.name.clone() },
                                a {
                                    class: "link link-info",
                                    {flag.name.as_str()}
                                }
                            }
                        }
                        td {
                            class: "text-nowrap",
                            {flag.created_at.format("%Y-%m-%d %H:%M:%S").to_string()}
                        }
                        td {
                            class: "text-nowrap",
                            {flag.updated_at.format("%Y-%m-%d %H:%M:%S").to_string()}
                        }
                        td {
                            class: "text-right",
                            div {
                                class: "badge text-white text-lg py-3",
                                class: if flag.enabled { "badge-success" } else { "badge-error" },

                                if flag.enabled { "Enabled" } else { "Disabled" }
                            }
                        }
                    }
                }
            }
        }
    }
}