use dioxus::prelude::*;
use crate::pages::flag::Flag;

#[component]
pub fn NewFlag() -> Element {
    rsx! {
        Flag{
            name: None
        }
    }
}