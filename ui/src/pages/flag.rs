use crate::components::distributions::Distributions;
use crate::components::export_dialog;
use crate::components::export_dialog::ExportDialog;
use crate::components::flag_control_buttons::FlagControlButtons;
use crate::components::flag_name_and_type::FlagNameAndType;
use crate::components::groups::Groups;
use crate::components::rules::Rules;
use crate::components::variants::Variants;
use crate::server_api::ServerApi;
use crate::types_flag::{Flag, Variant, VariantDistribution};
use crate::types_ui::FlagValidation;
use crate::{server_api, Route};
use dioxus::prelude::*;
use std::collections::HashMap;

#[component]
pub fn Flag(name: Option<String>) -> Element {
    let mut flag = use_signal(|| Flag::default());
    let mut flag_yaml = use_signal(String::new);
    let mut is_loaded = use_signal(|| false);
    let mut validation: Signal<HashMap<FlagValidation::UiComponent, String>> = use_signal(HashMap::new);
    let server_api = use_context::<ServerApi>();

    {
        // a new scope to reuse the same names for owned values
        to_owned![server_api, name];
        let _ = use_resource(move || {
            to_owned![server_api, name];
            async move {
                if let Some(name) = name {
                    if let Ok(data) = server_api.get_flag(&name).await {
                        flag.set(data);
                        is_loaded.set(true);
                    }
                }
            }
        });
    }

    if name.is_some() && !is_loaded() {
        return rsx! {};
    }

    rsx! {
        // control buttons
        FlagControlButtons{
            validation: validation.read().clone(),
            can_delete: name.is_some(),
            on_save: {
                to_owned![server_api, name];
                move |_| {
                    to_owned![server_api, name];
                    spawn(async move {
                        let is_new_name = name.as_ref().map_or(true, |name| *name != flag().name);
                        if let Ok(()) = server_api.save_flag(name, &flag()).await{
                            // if there is no original name or the name has changed, navigate to the new flag route
                            if is_new_name {
                                navigator().replace(Route::Flag {name: flag().name});
                            }
                        }
                    });
                }
            },
            on_export: move |_| {
                let dto = server_api::to_dto(&flag());
                flag_yaml.set(serde_yaml::to_string(&dto).unwrap());
                eval(&format!("document.getElementById('{}').showModal();", export_dialog::DIALOG_ID));
            },
            on_delete: {
                to_owned![server_api, name];
                move |_| {
                    to_owned![server_api, name];
                    spawn(async move {
                        if let Ok(()) = server_api.delete_flag(name.unwrap().as_str()).await{
                            navigator().replace(Route::Home {});
                        }
                    });
                }
            },
        }

        ExportDialog{
            text: flag_yaml()
        }

        // flag name and type
        FlagNameAndType{
            flag_name: flag().name,
            flag_type: flag().flag_type,
            enabled: flag().enabled,
            on_name_change: move |name| {
                match name {
                    Ok(name) => {
                        flag.write().name = name;
                        validation.write().remove(&FlagValidation::UiComponent::NameAndType);
                    },
                    Err(err) => {
                        validation.write().insert(FlagValidation::UiComponent::NameAndType, err);
                    }
                };
            },
            on_type_change: move |flag_type| flag.write().flag_type = flag_type,
            on_enabled_change: move |enabled| flag.write().enabled = enabled,
            on_hash_change: move |hash_seed| flag.write().hash_seed = hash_seed,
        }
        hr { class: "my-5" }

        // variants
        Variants {
            variants: flag().variants,
            flag_type: flag().flag_type,
            on_change: move |variants: Result<Vec<Variant>, String>| {
                match variants {
                    Ok(variants) => {
                        flag.write().variants = variants;
                        validation.write().remove(&FlagValidation::UiComponent::Variants);
                    },
                    Err(err) => {
                        validation.write().insert(FlagValidation::UiComponent::Variants, err);
                    }
                };
            }
        }
        hr { class: "my-5" }

        // default distributions
        Distributions {
            title: "Default distribution".to_string(),
            bold_title: true,
            distributions: flag.peek().default_distributions.clone(),
            variants: flag().variants,
            on_change: move |distributions: Result<Vec<VariantDistribution>, String>| {
                match distributions {
                    Ok(distributions) => {
                        flag.write().default_distributions = distributions;
                        validation.write().remove(&FlagValidation::UiComponent::DefaultDistribution);
                    },
                    Err(err) => {
                        validation.write().insert(FlagValidation::UiComponent::DefaultDistribution, err);
                    }
                };
            }
        }
        hr { class: "my-5" }

        // rules
        Rules{
            rules: flag().rules,
            variants: flag().variants,
            on_change: move |rules| {
                match rules {
                    Ok(rules) => {
                        flag.write().rules = rules;
                        validation.write().remove(&FlagValidation::UiComponent::Rule);
                    },
                    Err(err) => {
                        validation.write().insert(FlagValidation::UiComponent::Rule, err);
                    }
                };
            },
        }
        hr { class: "my-5" }

        // groups
        Groups {
            group_names: flag().groups,
            on_change: move |groups| flag.write().groups = groups,
        }
    }
}
