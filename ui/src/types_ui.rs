pub mod FlagValidation{
    #[derive(Eq, Hash, PartialEq, Clone)]
    pub enum UiComponent {
        NameAndType,
        Variants,
        DefaultDistribution,
        Rule
    }
}
