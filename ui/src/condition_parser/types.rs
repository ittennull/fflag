use std::fmt::Debug;

#[derive(Debug, Clone)]
pub(crate) enum Operator {
    IsEqualTo,
    IsNotEqualTo,
    GreaterThan,
    GreaterThanOrEqualTo,
    LessThan,
    LessThanOrEqualTo,
    In,
    NotIn,
}

#[derive(Debug, Clone)]
pub(crate) enum ConjunctionOperator {
    And,
    Or,
}

#[derive(Debug, PartialEq, Clone)]
pub enum ParsingErrorType {
    UnknownError,
    EmptyInput,
    ParenthesisMismatch,
    ExpectScalar,
    ExpectArray,
    ExpectOperandName,
    ExpectOperator,
    ExpectOperandValue,
    ExpectConjunctionOrGroupClosing,
}

#[derive(Debug, PartialEq, Clone)]
pub struct ParsingError<'a> {
    pub error_pointer: Option<&'a str>,
    pub error_type: ParsingErrorType,
}

impl<'a> ParsingError<'a> {
    pub fn new(error_type: ParsingErrorType, error_pointer: Option<&'a str>) -> Self {
        Self {
            error_pointer,
            error_type,
        }
    }
}
