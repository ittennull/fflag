use super::types::{ConjunctionOperator, Operator, ParsingError};
use crate::condition_parser::types::ParsingErrorType;
use common::flag::{Condition, ConditionValue, Parameter};
use nom::branch::alt;
use nom::bytes::complete::{tag, tag_no_case, take_while};
use nom::character::complete::{alphanumeric1, char, digit1, space0, space1};
use nom::combinator::{all_consuming, map_opt, opt, recognize, value};
use nom::error::{context, ContextError, ErrorKind, VerboseErrorKind};
use nom::multi::{many1, separated_list1};
use nom::sequence::{delimited, preceded, tuple};
use nom::{IResult, Parser};

type ParserError<'a> = nom::error::VerboseError<&'a str>;
type ParserResult<'a, T> = IResult<&'a str, T, ParserError<'a>>;

const ERR_OPERAND_NAME: &str = "operand_name";
const ERR_OPERATOR: &str = "operator";
const ERR_OPERAND_VALUE: &str = "operand_value";
const ERR_CONJUNCTION_OR_GROUP_CLOSING: &str = "conjunction_or_group_closing";
const ERR_EXPECT_SCALAR: &str = "expect_scalar";
const ERR_EXPECT_ARRAY: &str = "expect_array";

fn operator(input: &str) -> ParserResult<Operator> {
    let (rest, _) = space0(input)?;
    context(
        ERR_OPERATOR,
        alt((
            value(Operator::IsEqualTo, tag("==")),
            value(Operator::IsNotEqualTo, tag("!=")),
            value(Operator::GreaterThanOrEqualTo, tag(">=")),
            value(Operator::LessThanOrEqualTo, tag("<=")),
            value(Operator::GreaterThan, tag(">")),
            value(Operator::LessThan, tag("<")),
            value(Operator::In, tag_no_case("In")),
            value(Operator::NotIn, tag_no_case("NotIn")),
        )),
    )(rest)
}

fn operand_name(input: &str) -> ParserResult<Parameter> {
    let entity_id = value(Parameter::EntityId, tag("$EntityId"));
    let property_name = recognize(many1(alt((alphanumeric1, tag("_")))))
        .map(|s: &str| Parameter::Context(s.to_owned()));

    let name = alt((entity_id, property_name));

    let (rest, _) = space0(input)?;
    context(ERR_OPERAND_NAME, name)(rest)
}

fn operand_value(input: &str) -> ParserResult<ConditionValue> {
    let boolean = alt((
        value(ConditionValue::Bool(true), tag("true")),
        value(ConditionValue::Bool(false), tag("false")),
    ));

    let string = || {
        delimited(char('\"'), take_while(|c| c != '\"'), char('\"'))
            .map(|x: &str| ConditionValue::String(x.to_owned()))
    };

    let float = || {
        map_opt(
            recognize(preceded(opt(char('-')), tuple((digit1, char('.'), digit1)))),
            |s: &str| s.parse().map(ConditionValue::F64).ok(),
        )
    };

    let integer = || {
        map_opt(recognize(preceded(opt(char('-')), digit1)), |s: &str| {
            s.parse().map(ConditionValue::I64).ok()
        })
    };

    let array_separator = delimited(space0, char(','), space0);
    let array_value_parser = alt((string(), float(), integer()));
    let array = map_opt(
        delimited(
            tuple((char('['), space0)),
            separated_list1(array_separator, array_value_parser),
            tuple((space0, char(']'))),
        ),
        |vals| {
            let first = vals.first().unwrap();
            match first {
                ConditionValue::String(_) => vals
                    .into_iter()
                    .map(|x| match x {
                        ConditionValue::String(x) => Some(x),
                        _ => None,
                    })
                    .collect::<Option<Vec<_>>>()
                    .map(ConditionValue::VecOfString),
                ConditionValue::I64(_) => vals
                    .into_iter()
                    .map(|x| match x {
                        ConditionValue::I64(x) => Some(x),
                        _ => None,
                    })
                    .collect::<Option<Vec<_>>>()
                    .map(ConditionValue::VecOfI64),
                ConditionValue::F64(_) => vals
                    .into_iter()
                    .map(|x| match x {
                        ConditionValue::F64(x) => Some(x),
                        _ => None,
                    })
                    .collect::<Option<Vec<_>>>()
                    .map(ConditionValue::VecOfF64),
                _ => None,
            }
        },
    );

    let (rest, _) = space0(input)?;
    let any_value = alt((boolean, string(), float(), integer(), array));
    context(ERR_OPERAND_VALUE, any_value)(rest)
}

fn check_operator_and_value<'a>(
    input: &'a str,
    operator: &Operator,
    value: &ConditionValue,
) -> Result<(), nom::Err<ParserError<'a>>> {
    let create_error = |error| {
        nom::Err::Error(ParserError::add_context(
            input,
            error,
            nom::error::make_error(input, ErrorKind::SeparatedNonEmptyList),
        ))
    };

    let is_array = matches!(value, ConditionValue::VecOfString(_) | ConditionValue::VecOfI64(_)|ConditionValue::VecOfF64(_));

    match (operator, is_array) {
        (Operator::IsEqualTo, true)
        | (Operator::IsNotEqualTo, true)
        | (Operator::GreaterThan, true)
        | (Operator::GreaterThanOrEqualTo, true)
        | (Operator::LessThan, true)
        | (Operator::LessThanOrEqualTo, true) => Err(create_error(ERR_EXPECT_SCALAR)),
        (Operator::In, false) | (Operator::NotIn, false) => Err(create_error(ERR_EXPECT_ARRAY)),
        _ => Ok(()),
    }
}

fn parse_condition(input: &str) -> ParserResult<Condition> {
    let (rest, lhs_name) = operand_name(input)?;
    let (value_input, operator) = operator(rest)?;
    let (rest, rhs_value) = operand_value(value_input)?;

    check_operator_and_value(value_input, &operator, &rhs_value)?;

    let condition = match operator {
        Operator::IsEqualTo => Condition::IsEqualTo(lhs_name, rhs_value),
        Operator::IsNotEqualTo => Condition::IsNotEqualTo(lhs_name, rhs_value),
        Operator::GreaterThan => Condition::GreaterThan(lhs_name, rhs_value),
        Operator::GreaterThanOrEqualTo => Condition::GreaterThanOrEqualTo(lhs_name, rhs_value),
        Operator::LessThan => Condition::LessThan(lhs_name, rhs_value),
        Operator::LessThanOrEqualTo => Condition::LessThanOrEqualTo(lhs_name, rhs_value),
        Operator::In => Condition::In(lhs_name, rhs_value),
        Operator::NotIn => Condition::NotIn(lhs_name, rhs_value),
    };

    Ok((rest, condition))
}

fn parse_conjunction(input: &str) -> ParserResult<ConjunctionOperator> {
    let (rest, _) = space1(input)?;
    let (rest, conjunction_operator) = alt((
        value(ConjunctionOperator::And, tag("AND")),
        value(ConjunctionOperator::Or, tag("OR")),
    ))(rest)?;
    let (rest, _) = space1(rest)?;

    Ok((rest, conjunction_operator))
}

fn parse_line(input: &str) -> ParserResult<Condition> {
    let (rest, inner_group) = opt(delimited(space0, tag("("), space0))(input)?;
    let (rest, condition) = match inner_group {
        Some(_) => parse_line(rest)?,
        None => parse_condition(rest)?,
    };

    let (rest, conjunction) = context(
        ERR_CONJUNCTION_OR_GROUP_CLOSING,
        alt((
            parse_conjunction.map(Some),
            value(None, preceded(space0, tag(")"))),
            value(None, all_consuming(space0)),
        )),
    )(rest)?;

    let (rest, condition) = match conjunction {
        None => (rest, condition),
        Some(conjunction) => {
            let (rest, right_condition) = parse_line(rest)?;
            let condition = match conjunction {
                ConjunctionOperator::And => {
                    Condition::And(Box::new(condition), Box::new(right_condition))
                }
                ConjunctionOperator::Or => {
                    Condition::Or(Box::new(condition), Box::new(right_condition))
                }
            };
            (rest, condition)
        }
    };

    Ok((rest, condition))
}

fn get_parsing_error<'a>(input: &'a str, context: &str) -> ParsingError<'a> {
    let error_type = match context {
        ERR_OPERAND_NAME => ParsingErrorType::ExpectOperandName,
        ERR_OPERATOR => ParsingErrorType::ExpectOperator,
        ERR_OPERAND_VALUE => ParsingErrorType::ExpectOperandValue,
        ERR_CONJUNCTION_OR_GROUP_CLOSING => ParsingErrorType::ExpectConjunctionOrGroupClosing,
        ERR_EXPECT_SCALAR => ParsingErrorType::ExpectScalar,
        ERR_EXPECT_ARRAY => ParsingErrorType::ExpectArray,
        _ => panic!("Unknown error description: {}", context),
    };

    ParsingError::new(error_type, Some(input))
}

pub fn parse(input: &str) -> Result<Condition, ParsingError> {
    if input.trim().is_empty() {
        return Err(ParsingError::new(ParsingErrorType::EmptyInput, None));
    }

    let parenthesis_mismatches = input.chars().fold(0, |acc, x| {
        acc + match x {
            '(' => 1,
            ')' => -1,
            _ => 0,
        }
    });
    if parenthesis_mismatches != 0 {
        return Err(ParsingError::new(
            ParsingErrorType::ParenthesisMismatch,
            None,
        ));
    }

    match parse_line(input) {
        Ok((_, condition)) => Ok(condition),
        Err(nom::Err::Error(e)) | Err(nom::Err::Failure(e)) => {
            let last = e
                .errors
                .iter()
                .rfind(|(_, kind)| matches!(kind, VerboseErrorKind::Context(_)));

            match last {
                Some((input, VerboseErrorKind::Context(ctx))) => Err(get_parsing_error(input, ctx)),
                _ => Err(ParsingError::new(ParsingErrorType::UnknownError, None)),
            }
        }
        _ => Err(ParsingError::new(ParsingErrorType::UnknownError, None)),
    }
}
