#![allow(non_snake_case)]

mod components;
mod pages;
pub mod condition_parser;
mod types_flag;
mod server_api;
mod types_ui;

use crate::components::global_error::{GlobalError, GlobalErrorType};
use crate::components::navbar::NavBar;
use crate::pages::flag::Flag;
use crate::pages::home::Home;
use crate::pages::new_flag::NewFlag;
use crate::server_api::ServerApi;
use dioxus::prelude::*;
use futures_util::StreamExt;

pub fn App() -> Element {
    rsx! {
        Router::<Route> {}
    }
}

#[derive(Clone, Routable, Debug, PartialEq)]
#[rustfmt::skip]
pub enum Route {
    #[layout(Wrapper)]
        #[route("/")]
        Home {},
        #[route("/flag")]
        NewFlag {},
        #[route("/flag/:name")]
        Flag { name: String },
}

#[component]
fn Wrapper() -> Element {
    // error handling
    let mut error = use_signal(|| None);
    let on_error = use_coroutine(|mut rx: UnboundedReceiver<GlobalErrorType>| async move {
        while let Some(err) = rx.next().await {
            match err {
                GlobalErrorType::Api(msg) => error.set(Some(msg))
            }
        }
    });

    // create a ServerApi singleton that is available for all components from the context
    use_context_provider(|| ServerApi::new(on_error));

    rsx! {
        div{
            class: "container mx-auto",

            NavBar {}
            GlobalError {
                error: error
            }
            Outlet::<Route> {}
        }
    }
}
