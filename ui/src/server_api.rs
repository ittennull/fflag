use crate::components::global_error::GlobalErrorType;
use crate::types_flag::{Flag, FlagType, Rule, Variant, VariantDistribution};
use anyhow::{anyhow, Result};
use chrono::DateTime;
use common::flag as dto;
use dioxus::prelude::Coroutine;
use itertools::Itertools;
use gloo_net::http::{Request, Response};
use std::fmt::Debug;
use std::str::FromStr;
use uuid::Uuid;

const API_PREFIX: &str = "/api/flags";

#[derive(Clone)]
pub struct ServerApi {
    on_error: Coroutine<GlobalErrorType>,
}

impl ServerApi {
    pub fn new(on_error: Coroutine<GlobalErrorType>) -> Self {
        Self {
            on_error,
        }
    }

    pub async fn get_flags(&self) -> Result<Vec<Flag>> {
        let dtos = Request::get(API_PREFIX)
            .send().await?
            .json().await;
        let dtos: Vec<dto::Flag> = self.with_error_handling(dtos)?;
        let flags = dtos.into_iter()
            .map(from_dto)
            .sorted_by(|a, b| Ord::cmp(&b.created_at, &a.created_at))
            .collect();
        Ok(flags)
    }

    pub async fn get_flag(&self, flag_name: &str) -> Result<Flag> {
        let dtos = Request::get(&format!("{API_PREFIX}/{flag_name}"))
            .send().await?
            .json().await;
        let flag_dto: dto::Flag = self.with_error_handling(dtos)?;
        Ok(from_dto(flag_dto))
    }

    pub async fn save_flag(&self, original_name: Option<String>, flag: &Flag) -> Result<()> {
        let request_builder = match original_name {
            Some(name) => Request::put(&format!("{API_PREFIX}/{name}")),
            None => Request::post(API_PREFIX)
        };

        let response_result = request_builder
            .json(&to_dto(flag))?
            .send().await;
        let response = self.with_error_handling(response_result)?;
        self.check_response(response).await?;

        Ok(())
    }

    pub async fn import_flag(&self, yaml: String) -> Result<()> {
        let response_result = Request::post(API_PREFIX)
            .header("Content-Type", "application/yaml")
            .body(yaml)?
            .send().await;
        let response = self.with_error_handling(response_result)?;
        self.check_response(response).await?;

        Ok(())
    }

    pub async fn delete_flag(&self, name: &str) -> Result<()> {
        let response_result = Request::delete(&format!("{API_PREFIX}/{name}"))
            .send().await;
        let response = self.with_error_handling(response_result)?;
        self.check_response(response).await?;
        
        Ok(())
    }

    fn with_error_handling<T, E: ToString>(&self, result: Result<T, E>) -> Result<T, E> {
        if let Err(err) = &result {
            self.on_error.send(GlobalErrorType::Api(err.to_string()));
        }
        result
    }

    async fn check_response(&self, response: Response) -> Result<()> {
        if !response.ok() {
            let error = response.text().await?;
            self.on_error.send(GlobalErrorType::Api(error));
            return Err(anyhow!("API error"));
        }
        Ok(())
    }
}

fn from_dto(flag: dto::Flag) -> Flag {
    fn convert_variants<T: ToString>(vec: Vec<dto::Variant<T>>) -> Vec<Variant> {
        vec.into_iter()
            .map(|x| Variant {
                id: Uuid::new_v4().to_string(),
                name: x.name,
                value: x.value.to_string(),
            })
            .collect()
    }

    fn convert_distributions(
        variants: &[Variant],
        distribution: &[dto::Percent],
    ) -> Vec<VariantDistribution> {
        distribution
            .iter()
            .enumerate()
            .map(|(index, percent)| {
                VariantDistribution {
                    variant_id: variants[index].id.to_owned(),
                    percent: percent.0,
                }
            })
            .collect()
    }

    fn covert_rules(rules: Vec<dto::Rule>, variants: &Vec<Variant>) -> Vec<Rule> {
        rules
            .into_iter()
            .map(|rule| Rule {
                id: Uuid::new_v4().to_string(),
                name: rule.name,
                distributions: convert_distributions(&variants, &rule.distributions),
                condition: Some(rule.condition),
            })
            .collect()
    }

    let (flag_type, variants) = match flag.variants {
        dto::Variants::Bool(vec) => (FlagType::Bool, convert_variants(vec)),
        dto::Variants::String(vec) => (FlagType::String, convert_variants(vec)),
        dto::Variants::I64(vec) => (FlagType::I64, convert_variants(vec)),
        dto::Variants::F64(vec) => (FlagType::F64, convert_variants(vec)),
    };

    Flag {
        flag_type,
        name: flag.name,
        enabled: flag.enabled,
        default_distributions: convert_distributions(&variants, &flag.default_distributions),
        rules: covert_rules(flag.rules, &variants),
        variants,
        hash_seed: flag.hash_seed,
        groups: flag.groups,
        created_at: flag.created_at,
        updated_at: flag.updated_at,
    }
}

pub fn to_dto(flag: &Flag) -> dto::Flag {
    let convert_variants = |variants| {
        fn convert<T: FromStr>(variants: &[Variant]) -> Vec<dto::Variant<T>>
        where
            <T as FromStr>::Err: Debug,
        {
            variants
                .iter()
                .map(|x| dto::Variant {
                    name: x.name.to_owned(),
                    value: x.value.parse::<T>().unwrap(),
                })
                .collect()
        }

        match &flag.flag_type {
            FlagType::Bool => dto::Variants::Bool(convert(variants)),
            FlagType::String => dto::Variants::String(convert(variants)),
            FlagType::I64 => dto::Variants::I64(convert(variants)),
            FlagType::F64 => dto::Variants::F64(convert(variants)),
        }
    };

    fn convert_distributions(
        vec: &[VariantDistribution],
    ) -> Vec<dto::Percent> {
        vec.iter()
            .map(|x| dto::Percent(x.percent))
            .collect()
    }

    let rules = flag
        .rules
        .iter()
        .map(|rule| dto::Rule {
            name: rule.name.clone(),
            distributions: convert_distributions(&rule.distributions),
            condition: rule.condition.as_ref().unwrap().clone(),
        })
        .collect();

    dto::Flag {
        name: flag.name.clone(),
        enabled: flag.enabled,
        variants: convert_variants(&flag.variants),
        default_distributions: convert_distributions(&flag.default_distributions),
        rules,
        hash_seed: flag.hash_seed,
        groups: flag.groups.clone(),
        created_at: DateTime::default(), //server code will set this value by itself
        updated_at: DateTime::default(), //server code will set this value by itself
    }
}

