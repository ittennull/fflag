use chrono::{DateTime, Utc};
use common::flag::{Condition, DISTRIBUTION_PERCENT_MAX};
use std::fmt;
use std::fmt::{Display, Formatter};

#[derive(Clone, Copy, PartialEq)]
pub enum FlagType {
    Bool = 0,
    String,
    I64,
    F64,
}

#[derive(Clone, PartialEq)]
pub struct Flag {
    pub flag_type: FlagType,
    pub name: String,
    pub enabled: bool,
    pub variants: Vec<Variant>,
    pub default_distributions: Vec<VariantDistribution>,
    pub rules: Vec<Rule>,
    pub hash_seed: i64,
    pub groups: Vec<String>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(PartialEq, Default, Clone)]
pub struct Variant {
    pub id: String,
    pub name: String,
    pub value: String,
}

#[derive(Clone, PartialEq)]
pub struct VariantDistribution {
    pub variant_id: String,
    pub percent: u16,
}

#[derive(Clone, PartialEq)]
pub struct Rule {
    pub id: String,
    pub name: String,
    pub distributions: Vec<VariantDistribution>,
    pub condition: Option<Condition>,
}

impl Display for FlagType {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let str = match self {
            FlagType::Bool => "bool",
            FlagType::String => "string",
            FlagType::I64 => "integer",
            FlagType::F64 => "float",
        };
        f.write_str(str)
    }
}

impl From<&str> for FlagType {
    fn from(str: &str) -> Self {
        match str {
            "bool" => FlagType::Bool,
            "string" => FlagType::String,
            "integer" => FlagType::I64,
            "float" => FlagType::F64,
            _ => panic!("Can't convert {}", str),
        }
    }
}

impl VariantDistribution {
    pub fn new(id: &str) -> Self {
        Self {
            variant_id: id.to_string(),
            percent: 0,
        }
    }
}

impl Default for Flag {
    fn default() -> Self {
        Flag {
            flag_type: FlagType::Bool,
            name: "".to_string(),
            enabled: true,
            variants: default_bool_variants(),
            default_distributions: vec![
                VariantDistribution {
                    variant_id: "false".to_string(),
                    percent: DISTRIBUTION_PERCENT_MAX,
                },
                VariantDistribution {
                    variant_id: "true".to_string(),
                    percent: 0,
                },
            ],
            rules: vec![],
            hash_seed: rand::random(),
            groups: vec![],
            created_at: Default::default(),
            updated_at: Default::default(),
        }
    }
}

pub fn default_bool_variants() -> Vec<Variant> {
    vec![
        Variant {
            id: "false".to_string(),
            name: "False".to_string(),
            value: "false".to_string(),
        },
        Variant {
            id: "true".to_string(),
            name: "True".to_string(),
            value: "true".to_string(),
        },
    ]
}