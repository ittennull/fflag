use dioxus::prelude::*;
use dioxus_logger::tracing::{info, Level};
use ui::App;

fn main() {
    dioxus_logger::init(Level::WARN).expect("failed to init logger");
    info!("starting app");
    launch(App);
}
