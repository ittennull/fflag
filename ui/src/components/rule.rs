use std::cmp;
use dioxus::prelude::*;
use crate::components::distributions::Distributions;
use crate::components::parsing_error_description::ParsingErrorDescription;
use crate::condition_parser::parser;
use crate::condition_parser::types::{ParsingError, ParsingErrorType};
use crate::types_flag::{Rule, Variant, VariantDistribution};

struct RuleWithErrors{
    rule: Rule,
    condition_error: Option<String>,
    distribution_error: Option<String>,
}

#[derive(Clone)]
struct ErrorPointer {
    pub code_line: String,
    pub pointer: String,
    pub error_type: ParsingErrorType,
}

#[component]
pub fn Rule(
    rule: Rule,
    is_first: bool,
    is_last: bool,
    variants: Vec<Variant>,
    on_change: EventHandler<Result<Rule, String>>,
    on_move_up: EventHandler,
    on_move_down: EventHandler,
    on_delete: EventHandler,
) -> Element {
    let mut condition_text = use_signal(|| rule.condition.as_ref().map_or_else(|| "".to_string(), |x| x.to_string()));
    let mut error_pointer: Signal<Option<ErrorPointer>> = use_signal(|| None);
    let mut rule_name = use_signal(|| rule.name.clone());
    let mut rule_with_errors = use_signal(|| RuleWithErrors {
        rule: rule.clone(),
        condition_error: match rule.condition{
            Some(_) => None,
            None => Some("Condition is missing".to_string())
        },
        distribution_error: None,
    });

    rsx! {
        div {
            class: "mt-6 p-2 pl-4 rounded-box border shadow-md border-gray-400 relative",
            span {
                contenteditable: "true",
                class: "absolute -translate-y-5 border rounded-box px-3 bg-orange-100",
                oninput: move |event| rule_name.set(event.value()),
                onblur: move |_| {
                    rule_with_errors.write().rule.name = rule_name();
                    on_change.call(Ok(rule_with_errors.read().rule.clone()));
                },
                "{rule_with_errors.read().rule.name}",
            }
            div {
                class: "absolute flex gap-2 -translate-y-5 right-4",

                button {
                    class: "btn btn-circle btn-outline btn-neutral btn-xs bg-base-100 disabled:bg-base-100",
                    disabled: is_first,
                    title: "Move the rule up",
                    onclick: move |_| {
                        on_move_up.call(());
                    },
                    svg {
                        "stroke": "currentColor",
                        "fill": "none",
                        "viewBox": "0 0 24 24",
                        "xmlns": "http://www.w3.org/2000/svg",
                        "stroke-width": "1.5",
                        class: "size-4",
                        path {
                            "stroke-linecap": "round",
                            "stroke-linejoin": "round",
                            "d": "M4.5 10.5 12 3m0 0 7.5 7.5M12 3v18"
                        }
                    }
                }
                button {
                    class: "btn btn-circle btn-outline btn-neutral btn-xs bg-base-100 disabled:bg-base-100",
                    disabled: is_last,
                    title: "Move the rule down",
                    onclick: move |_| {
                        on_move_down.call(());
                    },
                    svg {
                        "xmlns": "http://www.w3.org/2000/svg",
                        "fill": "none",
                        "viewBox": "0 0 24 24",
                        "stroke-width": "1.5",
                        "stroke": "currentColor",
                        class: "size-4",
                        path {
                            "stroke-linecap": "round",
                            "stroke-linejoin": "round",
                            "d": "M19.5 13.5 12 21m0 0-7.5-7.5M12 21V3"
                        }
                    }
                }
                button {
                    class: "btn btn-circle btn-outline btn-error btn-xs bg-base-100 disabled:bg-base-100",
                    title: "Delete the rule",
                    onclick: move |_| {
                        on_delete.call(());
                    },
                    svg {
                        "fill": "none",
                        "xmlns": "http://www.w3.org/2000/svg",
                        "viewBox": "0 0 24 24",
                        "stroke-width": "1.5",
                        "stroke": "currentColor",
                        class: "size-4",
                        path {
                            "stroke-linecap": "round",
                            "d": "M6 18L18 6M6 6l12 12",
                            "stroke-linejoin": "round"
                        }
                    }
                }
            }
            div {
                class: "mt-4 flex items-center gap-2",
                label {
                    class: "input input-bordered flex-grow flex items-center gap-2",
                    class: if rule_with_errors.read().condition_error.is_some() { "input-error" },
                    span { class: "text-gray-400", "Condition:" }
                    input {
                        r#type: "text",
                        class: "grow",
                        placeholder: r#"for example: age > 18 OR country == "Netherlands""#,
                        value: "{condition_text}",
                        onchange: {
                            move |event| {
                                condition_text.set(event.value());
                                match parser::parse(&condition_text()) {
                                    Ok(condition) => {
                                        error_pointer.set(None);
                                        rule_with_errors.write().rule.condition = Some(condition);
                                        rule_with_errors.write().condition_error = None;
                                    }
                                    Err(error) => {
                                        error_pointer.set(get_error_pointer(&condition_text(), error));
                                        rule_with_errors.write().rule.condition = None;
                                        rule_with_errors.write().condition_error = Some("Condition is missing or invalid".to_string());
                                    }
                                };
                                call_on_change(rule_with_errors, on_change);
                            }
                        }
                    }
                }
                Link{
                    class: "btn btn-circle btn-outline btn-xs text-gray-400",
                    title: "Documentation and examples",
                    to: "https://ittennull.gitlab.io/fflag/create_flags.html#request-parameters-and-condition-operators",
                    "i"
                }
            }

            {
                match error_pointer(){
                    Some(ErrorPointer { code_line, pointer, error_type}) => rsx!{
                        div{
                            class: "mt-2 text-error whitespace-pre",
                            code{ "{code_line}" }
                            br{}
                            code{ "{pointer}" }
                        }

                        ParsingErrorDescription { error_type: error_type }
                    },
                    None => rsx!{}
                }
            }

            div{
                class: "mt-3",
                Distributions {
                    title: "Distribution",
                    bold_title: false,
                    distributions: rule_with_errors.read().rule.distributions.clone(),
                    variants: variants,
                    on_change: move |distributions: Result<Vec<VariantDistribution>, String>| {
                        match distributions{
                            Ok(distributions) => {
                                rule_with_errors.write().rule.distributions = distributions;
                                rule_with_errors.write().distribution_error = None;
                            },
                            Err(error) => {
                                rule_with_errors.write().distribution_error = Some(error);
                            }
                        }

                        call_on_change(rule_with_errors, on_change);
                    }
                }
            }
        }
    }
}

fn call_on_change(
    rule_with_errors: Signal<RuleWithErrors>,
    on_change: EventHandler<Result<Rule, String>>,
){
    let rule_with_errors = rule_with_errors.read();
    let error = rule_with_errors.distribution_error.as_ref().or(rule_with_errors.condition_error.as_ref()).clone();
    match error {
        Some(error) => on_change.call(Err(error.clone())),
        None => on_change.call(Ok(rule_with_errors.rule.clone())),
    }
}

fn get_error_pointer<'a>(condition_text: &'a str, parsing_error: ParsingError<'a>) -> Option<ErrorPointer> {
    const PREFIX_LENGTH: usize = 50;

    parsing_error.error_pointer.map(|pointer| {
        let index = condition_text.rfind(pointer).unwrap();
        let start = match index < PREFIX_LENGTH {
            true => 0,
            false => index - PREFIX_LENGTH,
        };

        let end = cmp::min(condition_text.len(), start + 2 * PREFIX_LENGTH);
        let code_line = condition_text[start..end].to_owned();
        let pointer = " ".repeat(index - start) + "^";
        ErrorPointer {
            code_line,
            pointer,
            error_type: parsing_error.error_type
        }
    })
}