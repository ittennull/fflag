use dioxus::prelude::*;
use crate::components::primitives::delete_button::DeleteButton;

#[component]
pub fn Groups(
    group_names: Vec<String>,
    on_change: EventHandler<Vec<String>>
) -> Element {

    let mut group_names = use_signal(|| group_names);

    rsx! {
        h3 { class: "font-bold text-lg", "Groups" }

        for (index, group_name) in group_names().into_iter().enumerate() {
            div {
                class: "flex flex-1 gap-5 mt-3 items-center",

                label { class: "input input-bordered flex items-center gap-2",
                    input {
                        r#type: "text",
                        class: "grow",
                        value: group_name,
                        onchange: move |event| {
                            group_names.write()[index] = event.value();
                            on_change.call(group_names());
                        }
                    }
                }

                DeleteButton{
                    onclick: move |_| {
                        group_names.write().remove(index);
                        on_change.call(group_names());
                    }
                }
            }
        }

        div { class: "mt-5",
            button {
                class: "btn btn-outline btn-sm",
                onclick: move |_| {
                    group_names.write().push("".to_string());
                },
                "Add"
            }
        }
    }
}