use dioxus::prelude::*;

pub const DIALOG_ID: &str = "export_dialog";

#[component]
pub fn ExportDialog(text: String) -> Element {
    rsx!{
        dialog {
            class: "modal",
            id: DIALOG_ID,
            div {
                class: "modal-box flex flex-col",
                h3 {
                    class: "text-lg font-bold",
                    "Export flag"
                }
                textarea{
                    class: "textarea textarea-bordered font-mono w-full mt-3 p-3",
                    "rows": 25,
                    "{text}"
                }

                button{
                    class: "btn btn-primary mt-3 self-center",
                    onclick: move |_| {
                        let text= text.clone();
                        spawn(async move {
                            let promise = web_sys::window().unwrap().navigator().clipboard().write_text(&text);
                            wasm_bindgen_futures::JsFuture::from(promise).await.unwrap();
                        });
                    },
                    "Copy to clipboard"
                }
            }
            form {
                method: "dialog",
                class: "modal-backdrop",
                button { "close" }
            }
        }
    }
}