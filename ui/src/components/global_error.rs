use dioxus::prelude::*;

pub enum GlobalErrorType {
    Api(String),
}

#[component]
pub fn GlobalError(error: Signal<Option<String>>) -> Element {
    rsx! {
        div{
            class: "alert alert-error mb-4 text-white",
            class: if error().is_some() { "visible" } else { "hidden" },
            img{
                src: "close.svg",
                class: "text-5xl w-8 cursor-pointer",
                onclick: move |_| error.set(None)
            }
            {error().unwrap_or_else(String::new)}
        }
    }
}