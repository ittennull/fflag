use dioxus::prelude::*;

#[component]
pub fn ErrorText(show_error: bool, children: Element) -> Element {
    rsx!{
        div{
            class: "mt-2 transition-opacity transition-height ease-out text-error duration-500" ,
            class: if show_error {"opacity-100 h-4"} else {"opacity-0 h-0"},
            {children}
        }
    }
}