use dioxus::prelude::*;
use uuid::Uuid;
use crate::components::rule::Rule;
use crate::types_flag::{Rule, Variant, VariantDistribution};

struct RuleWithError {
    rule: Rule,
    error: Option<String>,
}

#[component]
pub fn Rules(
    rules: Vec<Rule>,
    variants: Vec<Variant>,
    on_change: EventHandler<Result<Vec<Rule>, String>>
) -> Element {
    let mut rules_with_errors: Signal<Vec<_>> = use_signal(||
        rules
            .into_iter()
            .map(|rule| RuleWithError { rule, error: None })
            .collect());

    rsx! {
        h3 { class: "font-bold text-lg", "Rules" }

        for (index, rule_with_error) in rules_with_errors.read().iter().enumerate() {
            div{
                key: "{rule_with_error.rule.id}",
                Rule{
                    rule: rule_with_error.rule.clone(),
                    is_first: index == 0,
                    is_last: index == rules_with_errors.read().len() - 1,
                    variants: variants.clone(),
                    on_change: move |new_rule| {
                        match new_rule{
                            Ok(new_rule) => {
                                rules_with_errors.write()[index] = RuleWithError { rule: new_rule, error: None };
                            }
                            Err(error) => {
                                rules_with_errors.write()[index].error = Some(error);
                            }
                        }

                        call_on_change(&rules_with_errors.read(), on_change);
                    },
                    on_move_up: move |_| {
                        if index > 0 {
                            rules_with_errors.write().swap(index, index - 1);
                            call_on_change(&rules_with_errors.read(), on_change);
                        }
                    },
                    on_move_down: move |_| {
                        if index < rules_with_errors.read().len() - 1 {
                            rules_with_errors.write().swap(index, index + 1);
                            call_on_change(&rules_with_errors.read(), on_change);
                        }
                    },
                    on_delete: move |_| {
                        rules_with_errors.write().remove(index);
                        call_on_change(&rules_with_errors.read(), on_change);
                    }
                }
            }
        }

        div {
            class: "mt-5",
            button {
                class: "btn btn-outline btn-sm",
                onclick: move |_| {
                    let rule_name = format!("Rule {}", rules_with_errors.read().len() + 1);
                    rules_with_errors.write().push(RuleWithError {
                        rule: Rule{
                            id: Uuid::new_v4().to_string(),
                            name: rule_name,
                            condition: None,
                            distributions: variants.iter().map(|variant| VariantDistribution::new(&variant.id)).collect()
                        },
                        error: Some("Rule doesn't have a condition".to_string()) // default error until the flag is changed
                    });
                    call_on_change(&rules_with_errors.read(), on_change);
                },
                "Add"
            }
        }
    }
}

fn call_on_change(rules: &[RuleWithError], on_change: EventHandler<Result<Vec<Rule>, String>>){
    let error = rules.iter().filter_map(|x| x.error.as_ref()).next();
    if let Some(error) = error{
        on_change.call(Err(error.clone()));
        return;
    }

    on_change.call(Ok(rules.iter().map(|x| x.rule.clone()).collect()));
}