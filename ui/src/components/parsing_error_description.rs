use dioxus::prelude::*;
use crate::condition_parser::types::ParsingErrorType;

#[component]
pub fn ParsingErrorDescription(error_type: ParsingErrorType) -> Element {
    match error_type{
        ParsingErrorType::UnknownError => rsx! {"Unknown error, parsing failed"},
        ParsingErrorType::EmptyInput => rsx! {"Condition must be set"},
        ParsingErrorType::ParenthesisMismatch => rsx! {"Parenthesis mismatch: make sure number of opening parentheses equals number of closing parentheses"},
        ParsingErrorType::ExpectScalar => rsx! {
            "Scalar value is expected. For example: "
            code{
                class: "bg-base-300 p-1",
                "3.14"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "true"
            } ", "
            code{
                class: "bg-base-300 p-1",
                r#""Amsterdam""#
            }
            br{}
            "Arrays can only be used with operators "

            code{
                class: "bg-base-300 p-1",
                "In"
            } " and "
            code{
                class: "bg-base-300 p-1",
                "NotIn"
            }
        },
        ParsingErrorType::ExpectArray => rsx! {
            "Operators "
            code{
                class: "bg-base-300 p-1",
                "In"
            } " and "
            code{
                class: "bg-base-300 p-1",
                "NotIn"
            }
            " require a non-empty array value, for example  "
            code{
                class: "bg-base-300 p-1",
                "[1, 2, 3, 5]"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "[9.8, 5.0]"
            } ", "
            code{
                class: "bg-base-300 p-1",
                r#"["Rotterdam", "Almere"]"#
            }
        },
        ParsingErrorType::ExpectOperandName => rsx! {
            "Expected parameter name, for example: "

            code{
                class: "bg-base-300 p-1",
                "provinceName"
            } " or "
            code{
                class: "bg-base-300 p-1",
                "province_name"
            }
            br{}
            "There is one predefined name:"
            code{
                class: "bg-base-300 p-1",
                "$EntityId"
            }
        },
        ParsingErrorType::ExpectOperator => rsx! {
            "Expected operator, for example: "

            code{
                class: "bg-base-300 p-1",
                "=="
            } ", "
            code{
                class: "bg-base-300 p-1",
                "!="
            } ", "
            code{
                class: "bg-base-300 p-1",
                "<"
            } ", "
            code{
                class: "bg-base-300 p-1",
                ">"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "<="
            } ", "
            code{
                class: "bg-base-300 p-1",
                ">="
            } ", "
            code{
                class: "bg-base-300 p-1",
                "In"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "NotIn"
            }
        },
        ParsingErrorType::ExpectOperandValue => rsx! {
            "Expected a value, for example: "
            code{
                class: "bg-base-300 p-1",
                r#""Hello world""#
            } ", "
            code{
                class: "bg-base-300 p-1",
                "42"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "3.14"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "true"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "false"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "[2, 4, 8]"
            }
        },
        ParsingErrorType::ExpectConjunctionOrGroupClosing => rsx! {
            "Expected only: "
            code{
                class: "bg-base-300 p-1",
                "AND"
            } ", "
            code{
                class: "bg-base-300 p-1",
                "OR"
            } ", "
            code{
                class: "bg-base-300 p-1",
                ")"
            }
        }
    }
}