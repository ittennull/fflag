use dioxus::prelude::*;

#[component]
pub fn DeleteButton(onclick: EventHandler) -> Element {
    rsx!{
        button {
            class: "btn btn-circle btn-outline btn-error btn-sm",
            onclick: move |_| { onclick.call(()); },
            svg {
                "fill": "none",
                "xmlns": "http://www.w3.org/2000/svg",
                "viewBox": "0 0 24 24",
                "stroke": "currentColor",
                class: "h-6 w-6",
                path {
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    "d": "M6 18L18 6M6 6l12 12",
                    "stroke-width": "2"
                }
            }
        }
    }
}