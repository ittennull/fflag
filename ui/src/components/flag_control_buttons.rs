use std::collections::HashMap;
use dioxus::prelude::*;
use crate::types_ui::FlagValidation;

#[component]
pub fn FlagControlButtons(
    validation: HashMap<FlagValidation::UiComponent, String>,
    can_delete: bool,
    on_save: EventHandler<()>,
    on_export: EventHandler<()>,
    on_delete: EventHandler<()>,
) -> Element{
    rsx!{
        div { class: "flex justify-between items-center",
            div { class: "flex gap-3",
                button {
                    class: "w-32 btn btn-primary",
                    disabled: !validation.is_empty(),
                    title: if !validation.is_empty() { "{validation.iter().next().unwrap().1}" },
                    onclick: move |_| { on_save.call(()); },
                    "Save"
                }

                button {
                    class: "w-32 btn btn-outline",
                    disabled: !validation.is_empty(),
                    title: if !validation.is_empty() { "{validation.iter().next().unwrap().1}" },
                    onclick: move |_| { on_export.call(()); },
                    "Export"
                }
            }

            div {
                button {
                    class: "btn btn-outline btn-error",
                    class: if !can_delete { "invisible" },
                    onclick: move |_| { on_delete.call(()); },
                    "Delete"
                }
            }
        }
    }
}