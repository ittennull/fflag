use dioxus::prelude::*;
use itertools::Itertools;
use uuid::Uuid;
use crate::components::ErrorText::ErrorText;
use crate::components::primitives::delete_button::DeleteButton;
use crate::types_flag::{default_bool_variants, FlagType, Variant};

#[derive(Clone)]
enum AllVariantsValidationError{
    NotUniqueNames,
    NotUniqueValues
}

#[derive(Clone)]
enum ValidationNameError {
    EmptyName,
}

#[derive(Clone)]
enum ValidationValueError {
    NotNumber,
}

struct VariantWithError{
    variant: Variant,
    name_error: Option<ValidationNameError>,
    value_error: Option<ValidationValueError>,
}

#[component]
pub fn Variants(
    variants: Vec<Variant>,
    flag_type: FlagType,
    on_change: EventHandler<Result<Vec<Variant>, String>>) -> Element {

    let mut flag_type_signal = use_signal(|| flag_type);
    let mut variants = use_signal(|| variants_to_variants_with_error(variants));
    let all_variants_validation_error = use_signal(|| None);

    // outside of use_effect so that it doesn't become dependent on the signal and the update of the signal doesn't cause cyclic updates
    let remembered_flag_type = flag_type_signal();

    // set default variants when flag type changes. Remember the new flag type
    use_effect(use_reactive!(|flag_type| {
        if remembered_flag_type != flag_type {
            let new_variants = match flag_type {
                FlagType::Bool => variants_to_variants_with_error(default_bool_variants()),
                _ => vec![]
            };
            variants.set(new_variants);
            flag_type_signal.set(flag_type);
            validate_and_call_on_change(&variants.peek(), all_variants_validation_error, on_change);
        }
    }));

    let can_change_values = flag_type != FlagType::Bool;

    rsx! {
        h3 { class: "font-bold text-lg", "Variants" }

        ErrorText{
            show_error: all_variants_validation_error().is_some(),
            match all_variants_validation_error() {
                Some(AllVariantsValidationError::NotUniqueNames) => "Variant names must be unique",
                Some(AllVariantsValidationError::NotUniqueValues) => "Variant values must be unique",
                _ => ""
            }
        }

        for (index, variant_with_error) in variants.read().iter().enumerate() {
            div {
                key: "{variant_with_error.variant.id}",
                class: "flex flex-1 gap-5 mt-3 items-baseline",

                div{
                    label {
                        class: "input input-bordered flex items-center gap-2",
                        class: if variant_with_error.name_error.is_some() { "input-error" },

                        span { class: "text-gray-400", "Name:" }
                        input {
                            r#type: "text",
                            class: "grow",
                            value: "{variant_with_error.variant.name}",
                            oninput: {
                                move |event| {
                                    let new_name = event.value();
                                    variants.write()[index].name_error = validate_name(&new_name);
                                    variants.write()[index].variant.name = new_name;

                                    validate_and_call_on_change(&variants.read(), all_variants_validation_error, on_change);
                                }
                            }
                        }
                    }

                    ErrorText{
                        show_error: matches!(variant_with_error.name_error, Some(ValidationNameError::EmptyName)),
                        "Name cannot be empty",
                    }
                }

                if can_change_values{
                    div{
                        div{
                            class: "flex gap-5 items-center",

                            label {
                                class: "input input-bordered flex items-center gap-2 w-full max-w-lg",
                                class: if variant_with_error.value_error.is_some() { "input-error" },

                                span { class: "text-gray-400", "Value:" }
                                input {
                                    r#type: if matches!(flag_type, FlagType::String) { "text" } else { "number" },
                                    class: "grow",
                                    value: "{variant_with_error.variant.value}",
                                    oninput: {
                                        move |event| {
                                            let new_value = event.value();
                                            variants.write()[index].value_error = validate_value(&new_value, flag_type);
                                            variants.write()[index].variant.value = new_value;

                                            validate_and_call_on_change(&variants.read(), all_variants_validation_error, on_change);
                                        }
                                    }
                                }
                            }

                            if can_change_values{
                                DeleteButton{
                                    onclick: move |_| {
                                        variants.write().remove(index);
                                        validate_and_call_on_change(&variants.read(), all_variants_validation_error, on_change);
                                    }
                                }
                            }
                        }

                        ErrorText{
                            show_error: matches!(variant_with_error.value_error, Some(ValidationValueError::NotNumber)),
                            "Value is empty or not a number",
                        }
                    }
                }
                else{
                    span {
                        class: "text-gray-400",
                        code{
                            "Value: {variant_with_error.variant.value}",
                        }
                    }
                }
            }
        }

        if can_change_values{
            div {
                class: "mt-5",
                button {
                    class: "btn btn-outline btn-sm",
                    onclick: {
                        move |_| {
                            let mut new_item = VariantWithError{
                                variant: Variant{
                                    id: Uuid::new_v4().to_string(),
                                    name: "".to_string(),
                                    value: "".to_string(),
                                },
                                name_error: None,
                                value_error: None,
                            };
                            new_item.name_error = validate_name(&new_item.variant.name);
                            new_item.value_error = validate_value(&new_item.variant.value, flag_type);

                            variants.write().push(new_item);
                            validate_and_call_on_change(&variants.read(), all_variants_validation_error, on_change);
                        }
                    },
                    "Add"
                }
            }
        }
    }
}

fn validate_name(name: &String) -> Option<ValidationNameError>{
    name.is_empty().then_some(ValidationNameError::EmptyName)
}

fn validate_value(value: &String, flag_type: FlagType) -> Option<ValidationValueError>{
    match flag_type {
        FlagType::Bool => None,
        FlagType::String => None,
        FlagType::I64 => value.parse::<i64>().err().map(|_| ValidationValueError::NotNumber),
        FlagType::F64 => value.parse::<f64>().err().map(|_| ValidationValueError::NotNumber),
    }
}

fn validate_and_call_on_change(
    variants: &[VariantWithError],
    mut all_variants_validation_error: Signal<Option<AllVariantsValidationError>>,
    on_change: EventHandler<Result<Vec<Variant>, String>>) {

    let single_error = if let Some(err) = variants.iter().filter_map(|x| x.name_error.clone()).next(){
        match err {
            ValidationNameError::EmptyName => Some("Variant name must not be empty".to_string())
        }
    }
    else if let Some(err) = variants.iter().filter_map(|x| x.value_error.clone()).next(){
        match err {
            ValidationValueError::NotNumber => Some("Variant value is empty or not a number".to_string())
        }
    }
    else{
        None
    };

    let combined_error = if !variants.iter().map(|x| &x.variant.name).all_unique(){
        all_variants_validation_error.set(Some(AllVariantsValidationError::NotUniqueNames));
        Err("Variant names must be unique".to_string())
    }
    else if !variants.iter().map(|x| &x.variant.value).all_unique(){
        all_variants_validation_error.set(Some(AllVariantsValidationError::NotUniqueValues));
        Err("Variant values must be unique".to_string())
    }
    else{
        all_variants_validation_error.set(None);

        if let Some(err) = single_error{
            Err(err)
        }
        else{
            Ok(variants.iter().map(|x| x.variant.clone()).collect())
        }
    };

    on_change.call(combined_error);
}

fn variants_to_variants_with_error(variants: Vec<Variant>) -> Vec<VariantWithError> {
    variants.into_iter().map(|variant| VariantWithError {
        variant,
        name_error: None,
        value_error: None,
    })
        .collect()
}