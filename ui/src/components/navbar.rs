use dioxus::prelude::*;
use crate::Route;

#[component]
pub fn NavBar() -> Element {
    rsx! {
        div {
            class: "flex flex-1 items-center justify-center justify-between mb-5 px-5 py-3 bg-gradient-to-r from-sky-300 to-sky-100 rounded",
            div {
                class: "text-3xl text-white transition-colors duration-200 ease-in-out hover:text-indigo-900",

                Link {
                    to: Route::Home {},
                    "Feature flags"
                }
            }

            div {
                class: "flex gap-4 items-center",

                Link {
                    class: "flex flex-shrink-0 gap-2 items-center hover:text-indigo-900",
                    to: "https://ittennull.gitlab.io/fflag",

                    svg {
                        "xmlns": "http://www.w3.org/2000/svg",
                        "stroke": "currentColor",
                        "fill": "none",
                        "viewBox": "0 0 24 24",
                        "stroke-width": "1.5",
                        class: "size-6",
                        path {
                            "d": "M12 6.042A8.967 8.967 0 0 0 6 3.75c-1.052 0-2.062.18-3 .512v14.25A8.987 8.987 0 0 1 6 18c2.305 0 4.408.867 6 2.292m0-14.25a8.966 8.966 0 0 1 6-2.292c1.052 0 2.062.18 3 .512v14.25A8.987 8.987 0 0 0 18 18a8.967 8.967 0 0 0-6 2.292m0-14.25v14.25",
                            "stroke-linecap": "round",
                            "stroke-linejoin": "round"
                        }
                    }

                    "Documentation"
                }

                Link {
                    class: "whitespace-nowrap flex flex-shrink-0 items-center hover:text-indigo-900",
                    to: "https://gitlab.com/ittennull/fflag",

                    svg {
                        class: "w-10",
                        "xmlns": "http://www.w3.org/2000/svg",
                        "stroke": "currentColor",
                        "fill": "currentColor",
                        "viewBox": "0 0 380 380",
                        "stroke-width": "2",
                        g {
                            path {
                                "d": "M282.83,170.73l-.27-.69-26.14-68.22a6.81,6.81,0,0,0-2.69-3.24,7,7,0,0,0-8,.43,7,7,0,0,0-2.32,3.52l-17.65,54H154.29l-17.65-54A6.86,6.86,0,0,0,134.32,99a7,7,0,0,0-8-.43,6.87,6.87,0,0,0-2.69,3.24L97.44,170l-.26.69a48.54,48.54,0,0,0,16.1,56.1l.09.07.24.17,39.82,29.82,19.7,14.91,12,9.06a8.07,8.07,0,0,0,9.76,0l12-9.06,19.7-14.91,40.06-30,.1-.08A48.56,48.56,0,0,0,282.83,170.73Z"
                            },
                            path {
                                "d": "M282.83,170.73l-.27-.69a88.3,88.3,0,0,0-35.15,15.8L190,229.25c19.55,14.79,36.57,27.64,36.57,27.64l40.06-30,.1-.08A48.56,48.56,0,0,0,282.83,170.73Z"
                            },
                            path {
                                "d": "M153.43,256.89l19.7,14.91,12,9.06a8.07,8.07,0,0,0,9.76,0l12-9.06,19.7-14.91S209.55,244,190,229.25C170.45,244,153.43,256.89,153.43,256.89Z"
                            },
                            path {
                                "d": "M132.58,185.84A88.19,88.19,0,0,0,97.44,170l-.26.69a48.54,48.54,0,0,0,16.1,56.1l.09.07.24.17,39.82,29.82s17-12.85,36.57-27.64Z"
                            }
                        }
                    }

                    "Source code"
                }
            }
        }
    }
}