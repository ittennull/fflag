use dioxus::prelude::*;

pub const DIALOG_ID: &str = "import_dialog";

#[component]
pub fn ImportDialog(on_import: EventHandler<String>) -> Element {
    let mut text = use_signal(String::new);

    rsx!{
        dialog {
            class: "modal",
            id: DIALOG_ID,
            div {
                class: "modal-box flex flex-col",
                h3 {
                    class: "text-lg font-bold",
                    "Import flag"
                }
                textarea{
                    class: "textarea textarea-bordered font-mono w-full mt-3 p-3",
                    "rows": 25,
                    onchange: move |e| {
                        text.set(e.value());
                    },
                    "{text}"
                }

                button{
                    class: "btn btn-primary mt-3 self-center",
                    onclick: move |_| {
                        on_import.call(text());
                        eval(&format!("document.getElementById('{DIALOG_ID}').close();"));
                    },
                    "Import"
                }
            }
            form {
                method: "dialog",
                class: "modal-backdrop",
                button { "close" }
            }
        }
    }
}