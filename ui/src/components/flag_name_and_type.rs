use std::fmt::{Display, Formatter};
use dioxus::prelude::*;
use crate::components::ErrorText::ErrorText;
use crate::types_flag::FlagType;

#[component]
pub fn FlagNameAndType(
    flag_name: String,
    flag_type: FlagType,
    enabled: bool,
    on_name_change: EventHandler<Result<String, String>>,
    on_type_change: EventHandler<FlagType>,
    on_enabled_change: EventHandler<bool>,
    on_hash_change: EventHandler<i64>,
) -> Element {
    let mut validated_name = use_signal(|| ValidatedName { flag_name, validation: NameValidation::Ok });
    let mut flag_type = use_signal(|| flag_type);

    rsx!{
        div { class: "flex flex-1 justify-between items-center mt-10",
            div{
                class: "flex flex-1 items-center gap-5",
                label {
                    class: "input input-bordered flex items-center gap-2 w-full max-w-lg",
                    class: if validated_name.read().validation != NameValidation::Ok { "input-error" },
                    span { class: "text-gray-400", "Flag name:" }
                    input {
                        r#type: "text",
                        class: "grow",
                        value: "{validated_name.read().flag_name}",
                        oninput: move |event| {
                            let flag_name = event.value();
                            let validation = validate_flag_name(&flag_name);
                            let result = match &validation{
                                NameValidation::Ok => Ok(flag_name.clone()),
                                x => Err(x.to_string()),
                            };

                            validated_name.set(ValidatedName { flag_name, validation });
                            on_name_change.call(result);
                        }
                    }
                }

                div { class: "flex gap-5 items-center",
                    span { "Type: " }
                    select {
                        class: "select select-bordered w-full max-w-md",
                        onchange: move |event| {
                            let value = event.value();
                            flag_type.set(value.as_str().into());
                            on_type_change.call(flag_type());
                        },
                        option { selected: matches!(flag_type(), FlagType::Bool),   value: "{FlagType::Bool}", "Boolean" }
                        option { selected: matches!(flag_type(), FlagType::I64),    value: "{FlagType::I64}", "Integer" }
                        option { selected: matches!(flag_type(), FlagType::F64),    value: "{FlagType::F64}", "Float" }
                        option { selected: matches!(flag_type(), FlagType::String), value: "{FlagType::String}", "String" }
                    }
                }

                div { class: "flex w-32",
                    input {
                        checked: "{enabled}",
                        r#type: "checkbox",
                        class: "toggle toggle-success mr-2 peer",
                        onchange: move |event| { on_enabled_change.call(event.checked()); }
                    }
                    span { class: "hidden peer-checked:block", "Enabled" }
                    span { class: "block peer-checked:hidden", "Disabled" }
                }
            }

            button {
                class: "btn btn-outline",
                onclick: move |_| { on_hash_change.call(rand::random()); },
                "Reset hashes"
            }
        }

        ErrorText{
            show_error: validated_name.read().validation != NameValidation::Ok,
            "{validated_name.read().validation}"
        }
    }
}

struct ValidatedName{
    flag_name: String,
    validation: NameValidation,
}

#[derive(PartialEq)]
enum NameValidation{
    Ok,
    Empty,
    InvalidSymbols,
}

impl Display for NameValidation {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            NameValidation::Empty => "The flag name can't be empty",
            NameValidation::InvalidSymbols => "Only ASCII alphanumeric characters, hyphens, and underscores are allowed in the flag name",
            _ => ""
        };
        f.write_str(str)
    }
}

fn validate_flag_name(flag_name: &str) -> NameValidation{
    if flag_name.is_empty(){
        NameValidation::Empty
    }
    else if flag_name.chars().any(|x| !x.is_ascii_alphanumeric() && x != '_' && x != '-'){
        NameValidation::InvalidSymbols
    }
    else{
        NameValidation::Ok
    }
}