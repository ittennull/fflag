use crate::types_flag::{Variant, VariantDistribution};
use common::flag::DISTRIBUTION_PERCENT_MAX;
use dioxus::prelude::*;
use std::ops::IndexMut;

#[derive(Clone)]
struct DistributionItem {
    variant_name: String,
    distribution: VariantDistribution,
}

struct ValidatedDistributions {
    distribution_items: Vec<DistributionItem>,
    percents_are_correct: bool,
}

#[component]
pub fn Distributions(
    title: String,
    bold_title: bool,
    distributions: Vec<VariantDistribution>,
    variants: Vec<Variant>,
    on_change: EventHandler<Result<Vec<VariantDistribution>, String>>,
) -> Element {
    let mut saved_variants = use_signal(|| variants.clone());
    let mut distributions = use_signal(|| {
        let mut distribution_items = vec![];
        for i in 0..variants.len() {
            distribution_items.push(DistributionItem {
                variant_name: variants[i].name.clone(),
                distribution: distributions[i].clone(),
            });
        }
        ValidatedDistributions {
            percents_are_correct: sums_up_to_100(&distribution_items),
            distribution_items,
        }
    });

    if !variants.iter().eq(saved_variants.peek().iter()) {
        let mut new_distributions = vec![];
        for variant in variants.iter() {
            let percent = distributions.peek()
                .distribution_items
                .iter()
                .find(|x| x.distribution.variant_id == variant.id)
                .map_or(0, |x| x.distribution.percent);

            new_distributions.push(DistributionItem {
                variant_name: variant.name.clone(),
                distribution: VariantDistribution {
                    variant_id: variant.id.clone(),
                    percent,
                },
            });
        }

        saved_variants.set(variants);
        distributions.set(ValidatedDistributions {
            percents_are_correct: sums_up_to_100(&new_distributions),
            distribution_items: new_distributions,
        });
        call_on_change(&distributions.peek(), on_change);
    }

    rsx! {
        h3 {
            class: "text-lg",
            class: if bold_title {"font-bold"},
            "{title}"
        }

        div{
            class: "mt-4 flex gap-5 items-center",

            table{
                tbody{
                    for (index, item) in distributions.read().distribution_items.iter().enumerate(){
                        tr{
                            td{
                                class: "pr-5",
                                "{item.variant_name}"
                            }
                            td{
                                class: "py-1",
                                label { class: "input input-bordered flex items-center gap-2",
                                    span { class: "text-gray-400", "%" }
                                    input {
                                        r#type: "number",
                                        min: "0",
                                        max: "100",
                                        step: "0.1",
                                        class: "grow",
                                        value: "{item.distribution.percent as f64 / 10.0}",
                                        oninput: {
                                            move |event| {
                                                if let Ok(value) = event.value().parse::<f64>(){
                                                    distributions.write().distribution_items.index_mut(index).distribution.percent = (value * 10.0) as u16;
                                                    let percents_are_correct = sums_up_to_100(&distributions.peek().distribution_items);
                                                    distributions.write().percents_are_correct = percents_are_correct;
                                                    call_on_change(&distributions.read(), on_change);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            div{
                class: "transition-opacity transition-colors ease-out",
                class: if distributions.read().percents_are_correct {"opacity-0 text-success duration-1000"} else {"opacity-100 text-error duration-300"},
                if !distributions.read().distribution_items.is_empty() {
                    "Sum of all percents: {distributions.read().distribution_items.iter().map(|x| x.distribution.percent as f64).sum::<f64>() / 10.0}%"
                }
            }
        }
    }
}

fn sums_up_to_100(distributions: &[DistributionItem]) -> bool {
    distributions.iter().map(|x| x.distribution.percent).sum::<u16>() == DISTRIBUTION_PERCENT_MAX
}

fn call_on_change(
    validated_distribution: &ValidatedDistributions,
    on_change: EventHandler<Result<Vec<VariantDistribution>, String>>,
) {
    if validated_distribution.percents_are_correct {
        on_change.call(Ok(validated_distribution.distribution_items.iter().map(|x| x.distribution.clone()).collect()));
    } else {
        on_change.call(Err("Sum of all percents must be 100%".to_string()));
    }
}