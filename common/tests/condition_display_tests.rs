use common::flag::Condition::*;
use common::flag::ConditionValue::*;
use common::flag::Parameter::*;

#[test]
fn equals() {
    let cond = IsEqualTo(EntityId, Bool(true));
    assert_eq!("$EntityId == true", cond.to_string())
}

#[test]
fn in_array() {
    let cond = In(Context("par".into()), VecOfI64(vec![1, 4, 56]));
    assert_eq!("par In [1, 4, 56]", cond.to_string())
}

#[test]
fn condition_and_or_and() {
    let cond = Or(
        Box::new(And(
            Box::new(IsEqualTo(Context("par1".into()), Bool(true))),
            Box::new(LessThan(Context("par2".into()), I64(1))),
        )),
        Box::new(And(
            Box::new(GreaterThan(Context("par3".into()), F64(1.23))),
            Box::new(IsEqualTo(Context("par4".into()), I64(1))),
        )),
    );

    assert_eq!(
        "(par1 == true AND par2 < 1) OR (par3 > 1.23 AND par4 == 1)",
        cond.to_string()
    )
}

#[test]
fn condition_or_or_or() {
    let cond = Or(
        Box::new(Or(
            Box::new(IsEqualTo(Context("par1".into()), Bool(true))),
            Box::new(LessThan(Context("par2".into()), I64(1))),
        )),
        Box::new(Or(
            Box::new(GreaterThan(Context("par3".into()), F64(1.23))),
            Box::new(IsEqualTo(Context("par4".into()), I64(1))),
        )),
    );

    assert_eq!(
        "(par1 == true OR par2 < 1) OR (par3 > 1.23 OR par4 == 1)",
        cond.to_string()
    )
}

#[test]
fn condition_or_or_or2() {
    let cond = Or(
        Box::new(IsEqualTo(Context("par1".into()), Bool(true))),
        Box::new(Or(
            Box::new(LessThan(Context("par2".into()), I64(1))),
            Box::new(Or(
                Box::new(GreaterThan(Context("par3".into()), F64(1.23))),
                Box::new(IsEqualTo(Context("par4".into()), I64(1))),
            )),
        )),
    );

    assert_eq!(
        "par1 == true OR (par2 < 1 OR (par3 > 1.23 OR par4 == 1))",
        cond.to_string()
    )
}
