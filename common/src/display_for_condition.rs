use crate::flag::{Condition, ConditionValue, Parameter};
use std::fmt::{Display, Formatter};

impl Display for Condition {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(to_string(self, true).as_str())
    }
}

fn to_string(condition: &Condition, is_first_level: bool) -> String {
    match condition {
        Condition::And(left, right) => {
            let left = to_string(left, false);
            let right = to_string(right, false);
            match is_first_level {
                true => format!("{} AND {}", left, right),
                false => format!("({} AND {})", left, right),
            }
        }
        Condition::Or(left, right) => {
            let left = to_string(left, false);
            let right = to_string(right, false);
            match is_first_level {
                true => format!("{} OR {}", left, right),
                false => format!("({} OR {})", left, right),
            }
        }
        Condition::IsEqualTo(parameter, value) => {
            format!("{} == {}", parameter, value)
        }
        Condition::IsNotEqualTo(parameter, value) => {
            format!("{} =! {}", parameter, value)
        }
        Condition::GreaterThan(parameter, value) => {
            format!("{} > {}", parameter, value)
        }
        Condition::GreaterThanOrEqualTo(parameter, value) => {
            format!("{} >= {}", parameter, value)
        }
        Condition::LessThan(parameter, value) => {
            format!("{} < {}", parameter, value)
        }
        Condition::LessThanOrEqualTo(parameter, value) => {
            format!("{} <= {}", parameter, value)
        }
        Condition::In(parameter, value) => {
            format!("{} In {}", parameter, value)
        }
        Condition::NotIn(parameter, value) => {
            format!("{} NotIn {}", parameter, value)
        }
    }
}

impl Display for Parameter {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Parameter::EntityId => f.write_str("$EntityId"),
            Parameter::Context(name) => f.write_str(name.as_str()),
        }
    }
}

impl Display for ConditionValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ConditionValue::Bool(x) => write!(f, "{}", x),
            ConditionValue::String(x) => write!(f, r#""{}""#, x),
            ConditionValue::I64(x) => write!(f, "{}", x),
            ConditionValue::F64(x) => write!(f, "{}", x),
            ConditionValue::VecOfString(x) => write!(f, "{:?}", x),
            ConditionValue::VecOfI64(x) => write!(f, "{:?}", x),
            ConditionValue::VecOfF64(x) => write!(f, "{:?}", x),
        }
    }
}
