use chrono::{DateTime, Utc};

mod display_for_condition;
pub mod flag;

fn is_default(time: &DateTime<Utc>) -> bool {
    *time == (DateTime::<Utc>::default())
}
