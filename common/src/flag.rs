use super::is_default;
use chrono::prelude::*;
use serde::{Deserialize, Serialize};

pub const DISTRIBUTION_PERCENT_MAX: u16 = 1000;

#[derive(Serialize, Deserialize)]
pub struct Flag {
    #[serde(alias = "_id")]
    pub name: String,
    pub enabled: bool,
    pub variants: Variants,
    pub default_distributions: Vec<Percent>,
    pub rules: Vec<Rule>,
    pub hash_seed: i64,
    pub groups: Vec<String>,

    #[serde(default)]
    #[serde(skip_serializing_if = "is_default")]
    pub created_at: DateTime<Utc>,

    #[serde(default)]
    #[serde(skip_serializing_if = "is_default")]
    pub updated_at: DateTime<Utc>,
}

#[derive(Serialize, Deserialize)]
pub enum Variants {
    Bool(Vec<Variant<bool>>),
    String(Vec<Variant<String>>),
    I64(Vec<Variant<i64>>),
    F64(Vec<Variant<f64>>),
}

#[derive(Serialize, Deserialize)]
pub struct Variant<T> {
    pub name: String,
    pub value: T,
}

#[derive(Serialize, Deserialize)]
pub struct Percent(pub u16); // 0..DISTRIBUTION_PERCENT_MAX is same as 0%..100%

#[derive(Serialize, Deserialize)]
pub struct Rule {
    pub name: String,
    pub distributions: Vec<Percent>,
    pub condition: Condition,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Condition {
    And(Box<Condition>, Box<Condition>),
    Or(Box<Condition>, Box<Condition>),
    IsEqualTo(Parameter, ConditionValue),
    IsNotEqualTo(Parameter, ConditionValue),
    GreaterThan(Parameter, ConditionValue),
    GreaterThanOrEqualTo(Parameter, ConditionValue),
    LessThan(Parameter, ConditionValue),
    LessThanOrEqualTo(Parameter, ConditionValue),
    In(Parameter, ConditionValue),
    NotIn(Parameter, ConditionValue),
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Parameter {
    EntityId,
    Context(String),
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum ConditionValue {
    Bool(bool),
    String(String),
    I64(i64),
    F64(f64),
    VecOfString(Vec<String>),
    VecOfI64(Vec<i64>),
    VecOfF64(Vec<f64>),
}
