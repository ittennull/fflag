namespace FflagClient;

public interface IFlagEvaluator
{
    /// <summary>
    /// Get flag value
    /// </summary>
    /// <param name="flagName">Flag name</param>
    /// <param name="entityId">Stickiness parameter - pass the same value to get the same result</param>
    /// <param name="parameters">Parameters to use in flag rules</param>
    /// <returns>Returns null if the flag is not found or disabled</returns>
    bool? GetBool(string flagName, string? entityId = null, ParameterList? parameters = null);
    
    /// <summary>
    /// Get flag value
    /// </summary>
    /// <param name="flagName">Flag name</param>
    /// <param name="entityId">Stickiness parameter - pass the same value to get the same result</param>
    /// <param name="parameters">Parameters to use in flag rules</param>
    /// <returns>Returns null if the flag is not found or disabled</returns>
    string? GetString(string flagName, string? entityId = null, ParameterList? parameters = null);
    
    /// <summary>
    /// Get flag value
    /// </summary>
    /// <param name="flagName">Flag name</param>
    /// <param name="entityId">Stickiness parameter - pass the same value to get the same result</param>
    /// <param name="parameters">Parameters to use in flag rules</param>
    /// <returns>Returns null if the flag is not found or disabled</returns>
    long? GetLong(string flagName, string? entityId = null, ParameterList? parameters = null);
    
    /// <summary>
    /// Get flag value
    /// </summary>
    /// <param name="flagName">Flag name</param>
    /// <param name="entityId">Stickiness parameter - pass the same value to get the same result</param>
    /// <param name="parameters">Parameters to use in flag rules</param>
    /// <returns>Returns null if the flag is not found or disabled</returns>
    double? GetDouble(string flagName, string? entityId = null, ParameterList? parameters = null);
}