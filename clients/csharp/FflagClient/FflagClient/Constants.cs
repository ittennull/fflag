namespace FflagClient;

public class Constants
{
    /// <summary>
    /// Name of the <see href="https://learn.microsoft.com/en-us/dotnet/api/system.diagnostics.metrics.meter">Meter</see>
    /// that can be monitored.
    /// <example>
    /// <code>
    /// services.AddOpenTelemetry()
    ///     .WithMetrics(opts =>
    ///     {
    ///         opts.AddMeter(Constants.MeterName);
    ///     });
    /// </code>
    /// </example>
    /// </summary>
    public static string MeterName = "FflagClient";
    
    internal static string UpdateCounter = "configuration_update";
}