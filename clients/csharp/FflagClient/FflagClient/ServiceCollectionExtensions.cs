﻿using FlagConfiguration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace FflagClient;

public static class ServiceCollectionExtensions
{
    public static IHttpClientBuilder AddFflagClient(this IServiceCollection services, Action<FflagOptions> configureOptions)
    {
        services.AddOptions<FflagOptions>()
            .Configure(configureOptions)
            .Validate(options => !string.IsNullOrWhiteSpace(options.ServerUri?.ToString()), $"Missing {nameof(FflagOptions.ServerUri)}. It should point to FFlag service")
            .Validate(options => options.WatchedGroups.Count != 0, $"At least one flag group must be watched. Add a group to {nameof(FflagOptions.WatchedGroups)} list");
        
        services
            .AddSingleton<ConfigurationState>()
            .AddSingleton<IFlagEvaluator, FlagEvaluator>();
        services.AddHostedService<ConfigurationUpdater>();
        
        return services.AddGrpcClient<FlagConfigurationService.FlagConfigurationServiceClient>(typeof(FlagConfigurationService.FlagConfigurationServiceClient).FullName!, (sp, options) =>
        {
            options.Address = sp.GetRequiredService<IOptions<FflagOptions>>().Value.ServerUri;
        });
    }
}