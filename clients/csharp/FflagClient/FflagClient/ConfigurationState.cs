using System.Collections.Frozen;

namespace FflagClient;

class ConfigurationState
{
    public required FrozenDictionary<string, Flag> FlagConfigurations { get; set; } = new Dictionary<string, Flag>().ToFrozenDictionary();
}