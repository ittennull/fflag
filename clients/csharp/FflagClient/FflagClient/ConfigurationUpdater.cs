using System.Collections.Frozen;
using System.Diagnostics.Metrics;
using System.Reflection;
using FlagConfiguration;
using Grpc.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FflagClient;

class ConfigurationUpdater(
    FlagConfigurationService.FlagConfigurationServiceClient client,
    ConfigurationState state,
    IMeterFactory meterFactory,
    IOptions<FflagOptions> options,
    ILogger<ConfigurationUpdater> logger) : BackgroundService
{
    private static readonly string AppName = Assembly.GetEntryAssembly()?.GetName().Name ?? "Unknown";
    
    private readonly Counter<long> _counter = meterFactory.Create(Constants.MeterName)
        .CreateCounter<long>(Constants.UpdateCounter,  description: "Outcome of regular configuration synchronizations with the FFlag server");
    
    private readonly List<Group> _groups = options.Value.WatchedGroups.Distinct().Select(name => new Group { Name = name }).ToList();

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        logger.LogInformation("Starting watching flags in groups: {@groups}", _groups.Select(x => x.Name));
        
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                var request = new GetNewConfigurationRequest { KnownGroups = { _groups } };
                var response = await client.GetNewConfigurationAsync(request, cancellationToken: stoppingToken);

                UpdateFlags(response);
                AddOutcome(true);
            }
            catch (RpcException e)
            {
                logger.LogError(e, "Failed to fetch new configuration");
                AddOutcome(false);
            }
            
            await Task.Delay(TimeSpan.FromSeconds(options.Value.UpdateIntervalInSeconds), stoppingToken);
        }
    }

    private void UpdateFlags(GetNewConfigurationResponse response)
    {
        if (response.Groups.Count == 0)
            return;

        var flagNames = response.FlagConfigurations.Select(x => x.Name).ToArray();
        logger.LogInformation("New group configuration: {@groups}; flags of these groups: {@flagNames}", response.Groups, flagNames);

        foreach (var updatedGroup in response.Groups)
        {
            if (_groups.Find(x => x.Name == updatedGroup.Name) is { } group)
            {
                group.Version = updatedGroup.Version;
            }
        }

        var updatedGroupNames = response.Groups.Select(x => x.Name).ToHashSet();
        var newFlags = new List<Flag>();
        foreach (var existingFlag in state.FlagConfigurations.Values)
        {
            // copy flags that were not updated (are not in the response)
            if (!updatedGroupNames.Overlaps(existingFlag.Groups))
                newFlags.Add(existingFlag);
        }
        newFlags.AddRange(response.FlagConfigurations.Where(x => x.Enabled));
        
        logger.LogDebug("{0} flag configurations are in memory", newFlags.Count);

        state.FlagConfigurations = newFlags.ToDictionary(x => x.Name).ToFrozenDictionary();
    }

    private void AddOutcome(bool success)
    {
        _counter.Add(1,
            KeyValuePair.Create<string, object?>("client", AppName),
            KeyValuePair.Create<string, object?>("result", success ? "success" : "failure"));
    }
}