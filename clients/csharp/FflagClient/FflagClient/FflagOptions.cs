namespace FflagClient;

/// <summary>
/// Required parameters for the feature flag client
/// </summary>
public class FflagOptions
{
    /// <summary>
    /// GRPC uri of the feature flag server. By default, GRPC uses port 9090
    /// </summary>
    public required Uri ServerUri { get; set; }
    
    /// <summary>
    /// List of flag groups to watch. The flag must be assigned to one of these groups to be watched.
    /// The groups are assigned in the FFlag UI
    /// </summary>
    public required List<string> WatchedGroups { get; set; } = [];
    
    /// <summary>
    /// How often the client checks for the configuration updates
    /// </summary>
    public uint UpdateIntervalInSeconds { get; set; } = 60;
}
