using System.Text;
using FlagConfiguration;
using Standart.Hash.xxHash;

namespace FflagClient;

using Operation = Condition.ConditionsOneofCase;
using ConditionCase = Condition.ConditionsOneofCase;
using ConditionValueCase = ConditionValue.ConditionValuesOneofCase;
using VariantCase = Variants.VariantsOneofCase;

class FlagEvaluator(ConfigurationState state) : IFlagEvaluator
{
    internal const uint DistributionPercentMax = 1000;
    
    public bool? GetBool(string flagName, string? entityId = null, ParameterList? parameters = null)
    {
        var (flag, variantIndex) = GetVariantIndex(flagName, VariantCase.Bools, entityId, parameters);
        return flag?.Variants.Bools.Values[variantIndex];
    }
    
    public string? GetString(string flagName, string? entityId = null, ParameterList? parameters = null)
    {
        var (flag, variantIndex) = GetVariantIndex(flagName, VariantCase.Strings, entityId, parameters);
        return flag?.Variants.Strings.Values[variantIndex];
    }
    
    public long? GetLong(string flagName, string? entityId = null, ParameterList? parameters = null)
    {
        var (flag, variantIndex) = GetVariantIndex(flagName, VariantCase.I64S, entityId, parameters);
        return flag?.Variants.I64S.Values[variantIndex];
    }
    
    public double? GetDouble(string flagName, string? entityId = null, ParameterList? parameters = null)
    {
        var (flag, variantIndex) = GetVariantIndex(flagName, VariantCase.F64S, entityId, parameters);
        return flag?.Variants.F64S.Values[variantIndex];
    }

    private (Flag? Flag, int VariantIndex) GetVariantIndex(string flagName, VariantCase expectedType, string? entityId = null, ParameterList? parameters = null)
    {
        var flags = state.FlagConfigurations;
        
        if (!flags.TryGetValue(flagName, out var flag) || !flag.Enabled)
            return (null, -1);
        
        if (flag.Variants.VariantsCase != expectedType)
            throw new ArgumentException($"Requested flag type '{TypeName(expectedType)}' doesn't match the actual type '{TypeName(flag.Variants.VariantsCase)}'");

        var distributions = GetDistributions(flag, entityId, parameters);
        var percent = GetPercent(flag, entityId) % DistributionPercentMax;
        var variantIndex = GetVariantIndex(percent, distributions);
        return (flag, variantIndex);
    }

    private int GetVariantIndex(uint percent, IList<uint> distributions)
    {
        uint acc = 0;
        for (var i = 0; i < distributions.Count; i++)
        {
            acc += distributions[i];
            if (percent < acc)
                return i;
        }

        throw new Exception($"Couldn't find a variant for value '{percent}' among distribution: {string.Join(',', distributions)}");
    } 

    private uint GetPercent(Flag flag, string? entityId)
    {
        if (entityId != null)
        {
            var buffer = Encoding.UTF8.GetBytes($"{flag.Name}{entityId}");
            var hash = xxHash3.ComputeHash(buffer, buffer.Length, (ulong)flag.HashSeed);
            return (uint)(hash % ushort.MaxValue);
        }

        return (uint)Random.Shared.Next();
    }
    
    private IList<uint> GetDistributions(Flag flag, string? entityId, ParameterList? parameters)
    {
        for (var i = 0; i < flag.Rules.Count; i++)
        {
            var rule = flag.Rules[i];
            
            if (FulfillCondition(rule.Condition, entityId, parameters))
                return rule.Distributions;
        }

        return flag.DefaultDistributions;
    }
    
    private bool FulfillCondition(Condition condition, string? entityId, ParameterList? parameters)
    {
        return condition.ConditionsCase switch
        {
            ConditionCase.And => FulfillCondition(condition.And.Left, entityId, parameters) &&
                                 FulfillCondition(condition.And.Right, entityId, parameters),
            ConditionCase.Or => FulfillCondition(condition.Or.Left, entityId, parameters) ||
                                FulfillCondition(condition.Or.Right, entityId, parameters),
            ConditionCase.IsEqualTo => Check(condition.ConditionsCase, condition.IsEqualTo, entityId, parameters),
            ConditionCase.IsNotEqualTo => Check(condition.ConditionsCase, condition.IsNotEqualTo, entityId, parameters),
            ConditionCase.GreaterThan => Check(condition.ConditionsCase, condition.GreaterThan, entityId, parameters),
            ConditionCase.GreaterThanOrEqualTo => Check(condition.ConditionsCase, condition.GreaterThanOrEqualTo, entityId, parameters),
            ConditionCase.LessThan => Check(condition.ConditionsCase, condition.LessThan, entityId, parameters),
            ConditionCase.LessThanOrEqualTo => Check(condition.ConditionsCase, condition.LessThanOrEqualTo, entityId, parameters),
            ConditionCase.In => Check(condition.ConditionsCase, condition.In, entityId, parameters),
            ConditionCase.NotIn => Check(condition.ConditionsCase, condition.NotIn, entityId, parameters),
            _ => throw new ArgumentOutOfRangeException(nameof(condition.ConditionsCase), condition.ConditionsCase, null)
        };
    }

    private bool Check(Operation operation, Condition.Types.ParameterAndConditionValue parameterAndConditionValue, string? entityId, ParameterList? parameters)
    {
        if (parameterAndConditionValue.Parameter.IsEntityId)
        {
            if (entityId == null)
                return false;
            
            if (operation is Operation.In or Operation.NotIn)
                return CheckArray(entityId, operation, parameterAndConditionValue.ConditionValue.Strings.Values);

            return CheckSingleValue(entityId, operation, parameterAndConditionValue.ConditionValue.String);
        }
        else
        {
            if (parameters?.Values.TryGetValue(parameterAndConditionValue.Parameter.ContextValue, out var parameter) is not true)
                return false;
            
            if (operation is Operation.In or Operation.NotIn)
                return CheckArray(parameter, operation, parameterAndConditionValue.ConditionValue);
            
            return CheckSingleValue(parameter, operation, parameterAndConditionValue.ConditionValue);
        }
    }

    private bool CheckArray(Parameter parameter, Operation operation, ConditionValue conditionValue)
    {
        return conditionValue.ConditionValuesCase switch
        {
            ConditionValueCase.Strings => CheckArray(AsString(parameter), operation, conditionValue.Strings.Values),
            ConditionValueCase.I64S => CheckArray(AsLong(parameter), operation, conditionValue.I64S.Values),
            ConditionValueCase.F64S => CheckArray(AsDouble(parameter), operation, conditionValue.F64S.Values),
            _ => throw new ArgumentOutOfRangeException(nameof(conditionValue.ConditionValuesCase), conditionValue.ConditionValuesCase, null)
        };
    }
    
    private bool CheckArray<T>(T value, Operation operation, IList<T> array)
    {
        var isIn = array.Contains(value);

        return operation switch
        {
            Operation.In => isIn,
            Operation.NotIn => !isIn,
            _ => throw new ArgumentOutOfRangeException(nameof(operation), operation, null)
        };
    }

    private bool CheckSingleValue(Parameter parameter, Operation operation, ConditionValue conditionValue)
    {
        return conditionValue.ConditionValuesCase switch
        {
            ConditionValueCase.Bool => CheckSingleValue(AsBool(parameter), operation, conditionValue.Bool),
            ConditionValueCase.String => CheckSingleValue(AsString(parameter), operation, conditionValue.String),
            ConditionValueCase.I64 => CheckSingleValue(AsLong(parameter), operation, conditionValue.I64),
            ConditionValueCase.F64 => CheckSingleValue(AsDouble(parameter), operation, conditionValue.F64),
            _ => throw new ArgumentOutOfRangeException(nameof(conditionValue.ConditionValuesCase), conditionValue.ConditionValuesCase, null)
        };
    }
    
    private bool CheckSingleValue<T>(T left, Operation operation, T right) where T : IComparable<T>
    {
        var result = left.CompareTo(right);
        return operation switch
        {
            Operation.IsEqualTo => result == 0,
            Operation.IsNotEqualTo => result != 0,
            Operation.GreaterThan => result > 0,
            Operation.GreaterThanOrEqualTo => result >= 0,
            Operation.LessThan => result < 0,
            Operation.LessThanOrEqualTo => result <= 0,
            _ => throw new ArgumentOutOfRangeException(nameof(operation), operation, null)
        };
    }
    
    bool AsBool(Parameter parameter) => parameter.Bool ?? throw GetException<bool>(parameter);
    long AsLong(Parameter parameter) => parameter.Long ?? throw GetException<long>(parameter);
    double AsDouble(Parameter parameter) => parameter.Double ?? throw GetException<double>(parameter);
    string AsString(Parameter parameter) => parameter.String ?? throw GetException<string>(parameter);
    
    ArgumentException GetException<TExpected>(Parameter parameter) =>
        new($"Parameter '{parameter.Name}' is expected to have type '{typeof(TExpected).Name}', but it is '{TypeName(parameter)}'");
  
    string TypeName(Parameter parameter) =>
        parameter switch
        {
            { Bool: not null } => nameof(Boolean),
            { String: not null } => nameof(String),
            { Long: not null } => nameof(Int64),
            { Double: not null } => nameof(Double),
            _ => throw new ArgumentOutOfRangeException(nameof(parameter))
        };
    
    string TypeName(VariantCase variantsCase) =>
        variantsCase switch
        {
            VariantCase.Bools => nameof(Boolean),
            VariantCase.Strings => nameof(String),
            VariantCase.I64S => nameof(Int64),
            VariantCase.F64S => nameof(Double),
            _ => throw new ArgumentOutOfRangeException(nameof(variantsCase), variantsCase, null)
        };
}
