using System.Runtime.CompilerServices;

namespace FflagClient;

public class Parameters
{
    private Parameters() { }
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public static ParameterList Add(bool? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        new ParameterList().Add(value, name);
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public static ParameterList Add(long? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        new ParameterList().Add(value, name);
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public static ParameterList Add(string? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        new ParameterList().Add(value, name);
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public static ParameterList Add(double? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        new ParameterList().Add(value, name);
}

public class ParameterList
{
    internal Dictionary<string, Parameter> Values { get; } = [];
    
    internal ParameterList() { }
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public ParameterList Add(bool? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        AddParameter(name, new(name, Bool: value));
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public ParameterList Add(long? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        AddParameter(name, new(name, Long: value));
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public ParameterList Add(string? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        AddParameter(name, new(name, String: value));
    
    /// <summary>
    /// Adds a parameter to be used in rule's condition of a flag.
    /// The name argument can be provided explicitly or implicitly, e.g.
    /// <code>
    /// var countryCode = "NL";
    /// Parameters.Add(countryCode); // name is "countryCode" and the value is "NL"
    /// // or it can be written explicitly:
    /// Parameters.Add("NL", "countryCode");
    /// </code>
    /// </summary>
    /// <param name="value">Value of the parameter</param>
    /// <param name="name">A case-sensitive name that matches one of the parameter names in a rule's condition</param>
    public ParameterList Add(double? value, [CallerArgumentExpression(nameof(value))] string name = null!) =>
        AddParameter(name, new(name, Double: value));

    private ParameterList AddParameter(string name, Parameter parameter)
    {
        if (parameter.Bool != null || parameter.String != null || parameter.Long != null || parameter.Double != null)
            Values.Add(name, parameter);
        return this;
    }
}

record Parameter(string Name, bool? Bool = null, string? String = null, long? Long = null, double? Double = null);