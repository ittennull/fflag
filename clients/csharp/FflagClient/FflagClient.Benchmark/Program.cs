﻿using System.Collections.Frozen;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using FflagClient;
using FlagConfiguration;
using Parameter = FlagConfiguration.Parameter;

var summary = BenchmarkRunner.Run<FflagClientBenchmark>();


[MemoryDiagnoser]
public class FflagClientBenchmark
{
    readonly FlagEvaluator _evaluator;
    
    public FflagClientBenchmark()
    {
        // a lengthy flag configuration. Luckily, flags are normally created in the nice UI and not by hand like here
        var flag = new FlagConfiguration.FlagConfiguration
        {
            Name = "useNewColorTheme",
            Enabled = true,
            Variants = new Variants
            {
                Bools = new Variants.Types.BoolVariants
                {
                    Values = { true, false }
                },
            },
            HashSeed = 123456789,
            DefaultDistributions = { 500, 500 },
            Rules =
            {
                new Rule
                {
                    Condition = new Condition
                    {
                        Or = new Condition.Types.TwoConditions
                        {
                            Left = new Condition
                            {
                                In = new Condition.Types.ParameterAndConditionValue
                                {
                                    Parameter = new Parameter { ContextValue = "countryCode" },
                                    ConditionValue = new ConditionValue
                                    {
                                        Strings = new ConditionValue.Types.VecOfString
                                        {
                                            Values = { "NL", "DE", "FR", "BE" }
                                        }
                                    }
                                }
                            },
                            Right = new Condition
                            {
                                GreaterThan = new Condition.Types.ParameterAndConditionValue
                                {
                                    Parameter = new Parameter { ContextValue = "age" },
                                    ConditionValue = new ConditionValue { I64 = 20 }
                                }
                            }
                        }
                    },
                    Distributions = { 0, 1000 }
                }
            }
        };

        var state = new ConfigurationState
        {
            FlagConfigurations = new Dictionary<string, FlagConfiguration.FlagConfiguration>
            {
                {flag.Name, flag}
            }.ToFrozenDictionary()
        };

        _evaluator = new FlagEvaluator(state);
    }
    
    [Benchmark]
    public bool RandomWithoutParameters()
    {
        return _evaluator.GetBool("useNewColorTheme")!.Value;
    }
    
    [Benchmark]
    public bool WithEntityId()
    {
        return _evaluator.GetBool("useNewColorTheme", "userId")!.Value;
    }
    
    [Benchmark]
    public bool WithEntityIdAndCountryParameter()
    {
        return _evaluator.GetBool("useNewColorTheme", "userId", Parameters.Add("NL", "countryCode"))!.Value;
    }
    
    [Benchmark]
    public bool WithCountryParameter()
    {
        return _evaluator.GetBool("useNewColorTheme", null, Parameters.Add("NL", "countryCode"))!.Value;
    }
}
