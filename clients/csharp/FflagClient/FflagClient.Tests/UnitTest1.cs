using System.Collections.Frozen;
using FlagConfiguration;

namespace FflagClient.Tests;

using Parameter = FlagConfiguration.Parameter;

public class UnitTest1
{
    [Fact(DisplayName = "Return null for a flag that does not exist")]
    public void Test1()
    {
        var evaluator = CreateEvaluator();

        Assert.Null(evaluator.GetBool("flagThatDoesNotExist"));
        Assert.Null(evaluator.GetString("flagThatDoesNotExist"));
        Assert.Null(evaluator.GetLong("flagThatDoesNotExist"));
        Assert.Null(evaluator.GetDouble("flagThatDoesNotExist"));
    }
    
    [Fact(DisplayName = "Return null for a disabled flag")]
    public void Test2()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", enabled: false);

        Assert.Null(evaluator.GetBool("flag1"));
        Assert.Null(evaluator.GetString("flag1"));
        Assert.Null(evaluator.GetLong("flag1"));
        Assert.Null(evaluator.GetDouble("flag1"));
    }
    
    [Fact(DisplayName = "Choose default distribution when conditions are not met")]
    public void Test3()
    {
        var evaluator = CreateEvaluatorForFlag("flag1");

        Assert.True(evaluator.GetBool("flag1"));
        Assert.True(evaluator.GetBool("flag1", null, Parameters.Add(10, "age")));
        Assert.True(evaluator.GetBool("flag1", null, Parameters.Add("UK", "countryCode")));
        Assert.True(evaluator.GetBool("flag1", null, Parameters.Add(10, "age").Add("UK", "countryCode")));
    }
    
    [Fact(DisplayName = "Choose the distribution from the rule when conditions are met")]
    public void Test4()
    {
        var evaluator = CreateEvaluatorForFlag("flag1");

        Assert.False(evaluator.GetBool("flag1", null, Parameters.Add(21, "age")));
        Assert.False(evaluator.GetBool("flag1", null, Parameters.Add("NL", "countryCode")));
        Assert.False(evaluator.GetBool("flag1", null, Parameters.Add(21, "age").Add("UK", "countryCode")));
        Assert.False(evaluator.GetBool("flag1", null, Parameters.Add(10, "age").Add("NL", "countryCode")));
    }
    
    [Fact(DisplayName = "Requests with entityId get consistent results")]
    public void Test5()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", defaultDistribution: [500, 500], ruleDistribution: [500, 500]);

        var results = new bool[100000];
        for (var i = 0; i < results.Length; i++)
        {
            results[i] = evaluator.GetBool("flag1", "userId")!.Value;
        }
        
        var first = results[0];
        Assert.All(results, x => Assert.Equal(first, x));
    }
    
    [Fact(DisplayName = "Requests with entityId that choose a rule get consistent results within the rule")]
    public void Test6()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", defaultDistribution: [500, 500], ruleDistribution: [500, 500]);

        var results = new bool[100000];
        for (var i = 0; i < results.Length; i++)
        {
            results[i] = evaluator.GetBool("flag1", "userId", Parameters.Add(50, "age"))!.Value;
        }
        
        var first = results[0];
        Assert.All(results, x => Assert.Equal(first, x));
    }
    
    [Fact(DisplayName = "Requests without entityId get random results")]
    public void Test7()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", defaultDistribution: [500, 500]);

        var results = new bool[100000];
        for (var i = 0; i < results.Length; i++)
        {
            results[i] = evaluator.GetBool("flag1")!.Value;
        }
        
        var trues = (double)results.Count(x => x) / results.Length;
        var falses = (double)results.Count(x => !x) / results.Length;
        Assert.True(trues is > 0.45 and < 0.55);
        Assert.True(falses is > 0.45 and < 0.55);
    }
    
    [Fact(DisplayName = "Requests without entityId get random results where one value is more likely than the other")]
    public void Test8()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", defaultDistribution: [800, 200]);

        var results = new bool[100000];
        for (var i = 0; i < results.Length; i++)
        {
            results[i] = evaluator.GetBool("flag1")!.Value;
        }
        
        var trues = (double)results.Count(x => x) / results.Length;
        var falses = (double)results.Count(x => !x) / results.Length;
        Assert.True(trues is > 0.15 and < 0.25);
        Assert.True(falses is > 0.75 and < 0.85);
    }
    
    [Fact(DisplayName = "Requests with entityId get results with probability according to the default distribution")]
    public void Test9()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", defaultDistribution: [800, 200]);

        var results = new bool[100000];
        for (var i = 0; i < results.Length; i++)
        {
            results[i] = evaluator.GetBool("flag1", i.ToString())!.Value;
        }
        
        var trues = (double)results.Count(x => x) / results.Length;
        var falses = (double)results.Count(x => !x) / results.Length;
        Assert.True(trues is > 0.15 and < 0.25);
        Assert.True(falses is > 0.75 and < 0.85);
    }
    
    [Fact(DisplayName = "Requests with entityId get results with probability according to the distribution of the rule")]
    public void Test10()
    {
        var evaluator = CreateEvaluatorForFlag("flag1", defaultDistribution: [500, 500], ruleDistribution: [800, 200]);

        var results = new bool[100000];
        for (var i = 0; i < results.Length; i++)
        {
            results[i] = evaluator.GetBool("flag1", i.ToString(), Parameters.Add(40, "age"))!.Value;
        }
        
        var trues = (double)results.Count(x => x) / results.Length;
        var falses = (double)results.Count(x => !x) / results.Length;
        Assert.True(trues is > 0.15 and < 0.25);
        Assert.True(falses is > 0.75 and < 0.85);
    }
    
    [Fact(DisplayName = "Throws if parameter has incorrect type")]
    public void Test11()
    {
        var evaluator = CreateEvaluatorForFlag("flag1");

        // "40" is a string, but age is expected to be a number
        var exception = Assert.Throws<ArgumentException>(() => evaluator.GetBool("flag1", null, Parameters.Add("40", "age")));
        Assert.Contains("is expected to have type", exception.Message);
        
        // true is a boolean, but countryCode is expected to be a string
        exception = Assert.Throws<ArgumentException>(() => evaluator.GetBool("flag1", null, Parameters.Add(true, "countryCode")));
        Assert.Contains("is expected to have type", exception.Message);
    }
    
    [Fact(DisplayName = "Throws if flag has a different type than requested")]
    public void Test12()
    {
        var evaluator = CreateEvaluatorForFlag("flag1");

        var exception = Assert.Throws<ArgumentException>(() => evaluator.GetLong("flag1"));
        Assert.Contains("doesn't match the actual type", exception.Message);
        
        exception = Assert.Throws<ArgumentException>(() => evaluator.GetDouble("flag1"));
        Assert.Contains("doesn't match the actual type", exception.Message);
        
        exception = Assert.Throws<ArgumentException>(() => evaluator.GetString("flag1"));
        Assert.Contains("doesn't match the actual type", exception.Message);
    }
    
    [Fact(DisplayName = "Flag value doesn't change when distribution increases")]
    public void Test13()
    {
        var start = 10u;
        var flag = CreateFlag("flag", true, [FlagEvaluator.DistributionPercentMax - start, start]);
        var evaluator =  CreateEvaluator(flag);

        // find entityId that falls into a small segment with value 'true'
        string? entityId = null;
        for (var i = 0; i < 1000000; i++)
        {
            entityId = i.ToString();
            if (evaluator.GetBool(flag.Name, entityId)!.Value)
                break;
        }
        
        Assert.NotNull(entityId);

        // increase distribution of 'true' value -> flag still returns 'true' for the same entityId
        for (var i = start + 1; i < FlagEvaluator.DistributionPercentMax; i++)
        {
            // set distribution to [FlagEvaluator.DistributionPercentMax - i, i]
            flag.DefaultDistributions[0] = FlagEvaluator.DistributionPercentMax - i;
            flag.DefaultDistributions[1] = i;
            
            Assert.True(evaluator.GetBool(flag.Name, entityId)!.Value);
        }
    }

    private FlagEvaluator CreateEvaluatorForFlag(string name, bool enabled = true, uint[]? defaultDistribution = null, uint[]? ruleDistribution = null)
    {
        var flag = CreateFlag(name, enabled, defaultDistribution, ruleDistribution);
        return CreateEvaluator(flag);
    }
    
    private FlagEvaluator CreateEvaluator(params FlagConfiguration.FlagConfiguration[] flags)
    {
        return new FlagEvaluator(new ConfigurationState
        {
            FlagConfigurations = flags.ToDictionary(f => f.Name).ToFrozenDictionary()
        });
    }
   
    private FlagConfiguration.FlagConfiguration CreateFlag(string name, bool enabled = true, uint[]? defaultDistribution = null, uint[]? ruleDistribution = null)
    {
        return new FlagConfiguration.FlagConfiguration
        {
            Name = name,
            Enabled = enabled,
            Variants = new Variants
            {
                Bools = new Variants.Types.BoolVariants
                {
                    Values = { false, true }
                },
            },
            HashSeed = 123456789,
            DefaultDistributions = { defaultDistribution ?? [0, 1000] },
            Rules =
            {
                new Rule
                {
                    Condition = new Condition
                    {
                        Or = new Condition.Types.TwoConditions
                        {
                            Left = new Condition
                            {
                                In = new Condition.Types.ParameterAndConditionValue
                                {
                                    Parameter = new Parameter { ContextValue = "countryCode" },
                                    ConditionValue = new ConditionValue
                                    {
                                        Strings = new ConditionValue.Types.VecOfString
                                        {
                                            Values = { "NL", "DE", "FR", "BE" }
                                        }
                                    }
                                }
                            },
                            Right = new Condition
                            {
                                GreaterThan = new Condition.Types.ParameterAndConditionValue
                                {
                                    Parameter = new Parameter { ContextValue = "age" },
                                    ConditionValue = new ConditionValue { I64 = 20 }
                                }
                            }
                        }
                    },
                    Distributions = { ruleDistribution ?? [1000, 0] }
                }
            }
        };
    }
}