# HTTP
Similar to Grpc there are single-flag request and a batch request

### Single flag

Request:

```json
POST /eval

{
    "name": "string",
    "entityId": "string",
    "context":{
        "parameter1": "value1",
        "parameter2": 123,
        "parameter3": true
    }
}
```

Response:
```json
{
    "value": 45
}
```

Flag `name` is required, both `entityId` and `context` fields are optional.

A context is key-value pairs where each key can be used in a rule's condition. For example if a `context` has a key "birthDate" with a value "2000-01-01", it can be used in a condition: `birthDate > "1998-05-12"`. The type of the value is inferred from the condition and not from the request. In this example the "birthDate" context value is expected to be a string.

If a requested flag doesn't exist or disabled the response is _404 (NOT_FOUND)_.

### Batch request
Request example:
```json
POST /batch_eval

[
    {
        "name": "FlagName1",
        "entityId": "user1"
    },
    {
        "name": "FlagName1",
        "entityId": "user2"
    },
    {
        "name": "FlagName2",
        "context": {
            "age": true,
            "gender": "male"
        }
    },
    {
        "name": "FlagName3"
    }
]
```

Response:
```json
[
    {
        "name": "FlagName1",
        "entity_id": "user1",
        "value": false
    },
    {
        "name": "FlagName1",
        "entity_id": "user2",
        "value": true
    },
    {
        "name": "FlagName2",
        "entity_id": null,
        "value": 123
    }
]
```

The request queries 4 flags, "FlagName1" is requested twice for different users, "FlagName2" has additional context values and "FlagName3" doesn't exist, so it's missing in the response
