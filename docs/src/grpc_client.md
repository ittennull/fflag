# Grpc

Download grpc contract [query.proto](https://gitlab.com/ittennull/fflag/-/blob/master/proto/query.proto). With it you
can generate a client in your favorite language, see
an [example](https://gitlab.com/ittennull/fflag/-/tree/master/examples/grpc_client_dotnet) for C#, although C# has a
better [client-side client](./client_side_client.md)

There are two flavors of queries: get a single flag value (`GetFlag`) and a batch query to get a list of flags (
`GetFlags`). The batch version is better suited for when you need to get several flags because there is only one
round-trip to FFlag.

### GetFlag

Let's take a look at the request `GetFlag`:

```scala
service Query {
    rpc GetFlag(FlagRequest) returns (FlagResponse);
}

message FlagRequest{
    string name = 1;
    string entityId = 2;
    map<string, Value> context = 3;
}

message Value{
    oneof oneOfValue{
        bool bool = 1;
        string string = 2;
        sint64 int = 3;
        double double = 4;
    }
}

message FlagResponse{
    Value value = 1;
}
```

`FlagRequest` contains the name of a flag, [entityId](entityId.md) and a context. A context is key-value pairs where
each key can be used in a rule's condition. For example if a `context` has a key "birthDate" with a value "2000-01-01",
it can be used in an condition: `birthDate > "1998-05-12"`. The request from the example will satisfy the rule.

The `Value` is one of the 4 predefined types and each parameter's type must correspond to the type from a condition
where it's used. For example, in the condition `birthDate > "1998-05-12"` the "birthDate" is expected to be a string.

If a requested flag doesn't exist or disabled the response is _5 (
NOT_FOUND)_ ([grpc status codes](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)).

### GetFlags

The contract for `GetFlags` is very similar:

```scala
service Query {
    rpc GetFlags(FlagsRequest) returns (FlagsResponse);
}

message FlagRequest{
    string name = 1;
    string entityId = 2;
    map<string, Value> context = 3;
}

message FlagsRequest{
    repeated FlagRequest requests = 1;
}

message FlagsResponse{
    repeated GetFlagResult results = 1;
}

message GetFlagResult{
    string name = 1;
    string entityId = 2;
    Value value = 3;
}
```

For each flag you want to retrieve you send the discussed above `FlagRequest` in the `FlagsRequest.requests` collection
which means that every flag can have completely different parameters and have nothing in common between each other.

The response is a collection `FlagsResponse.results` where each `GetFlagResult` corresponds to one of the requests.
Don't make any assumptions about the order in this collection. If a flag is disabled or doesn't exist, it's not present
in the returned collection at all. For example, if a request contains two flags and both are disabled, the returned
collection is empty.