# Stickiness and EntityId

You can pass a special parameter `entityId` with a request. You get the same flag value in a response as long as

- you pass the same value for `entityId`
- the other request parameters hit the same rule or stay on the default distribution
- the flag configuration doesn't change

Parameter `entityId` usually represents a user ID, although it can be anything else.
This behavior is useful when you want to have a consistent returned value no matter how many times you ask for the flag
value, your users won't see a new feature going on and off after each page refresh.

If a flag has rules and a request with `entityId` falls under one of the rules then to keep getting the same flag value
all future requests should satisfy the rule too. If a request changes parameters and doesn't fit the rule anymore, FFlag
will start looking for a new fitting rule or take default distribution, it would be as it was the very first request
with this `entityId`.

The rules refer to the `entityId` parameter with special token `$EntityId`, e.g. `$EntityId == "alice"`

The stickiness is achieved by using a hash function

```
hash_value = hash(flagName + entityId)
```

The stickiness produces the same result as long as the flag configuration is the same because the hash value depends on
constant flag name and `entityId` and is compared against the same distributions.
Once the flag configuration changes, the result can change too.

### Example

You added two new UI themes: red and green:

![](img/entityId1.png)

By default, your users see the "current" theme but there are 2 rules:

1. if a user has a paid subscription, they get to see one of the new themes and the current theme is disabled for them
2. users from the specified two cities get a 50% chance to see the green theme

Let's see how the flag value will depend on request parameters and `entityId`.
We send requests to `/eval` endpoint (see [HTTP client](./http_client.md))

#### 1.

```json
{
  "name": "UiTheme",
  "context": {
    "hasPaidSubscription": false,
    "city": "Amsterdam"
  }
}
```

None of the rules matches -> return "current" from the default distribution:

```json
{
  "value": "current"
}
```

#### 2.

```json
{
  "name": "UiTheme",
  "context": {
    "hasPaidSubscription": true,
    "city": "Amsterdam"
  }
}
```

The first rule matches -> return red or green theme randomly. If you send this request multiple times, then
approximately half of them will get red theme, the others - green one

#### 3.

```json
{
  "name": "UiTheme",
  "entityId": "UserId123",
  "context": {
    "hasPaidSubscription": true,
    "city": "Amsterdam"
  }
}
```

The first rule matches -> return red or green theme randomly. In contrast to the previous request, this one has
`entityId` parameter and the response will be the same no matter how many times you send this request.

#### 4.

```json
{
  "name": "UiTheme",
  "entityId": "UserId123",
  "context": {
    "hasPaidSubscription": false,
    "city": "Amsterdam"
  }
}
```

The user "UserId123" stopped paying and switched to a free subscription. It doesn't satisfy the condition of "Rule 1"
anymore, but it does satisfy "Rule 2", so the same hash value now is used to choose one of the available options from "
Rule 2". As before, repeating this request will result in the same flag value, let's say it's "current".

#### 5.

Change distribution of the "Rule 2"

![](img/entityId3.png)

Repeat the previous request. There is a chance that you will keep getting the value "current" but the flag configuration
has changed and the hash value is now compared against the new distribution. It's highly possible (90% against 10%) that
the result will switch to "green" and stick with it.

That's OK because you want 90% of users from Amsterdam to get the "green" theme. And if you increase the green theme to
100% - everybody should get it regardless of what they had before.

## EntityId is also a parameter

`$EntityId` can be used just like any other parameter in the conditions. For example, you developed a new feature - AI
assistant. You deployed it to production but don't want the end users to see it yet. Only developers can experiment with
it. Pass userId/userName as `entityId` parameter in your requests to this flag and with this configuration below only
developers "bob" and "alice" get to work with the assistant.

![](img/entityId2.png)

Once internal tests are over, you can enable the new feature for regular users in the default distribution or with a new
rule.

## Reset hash

The stickiness depends on the hash value that depends on a seed. The button "Reset hashes" chooses a new seed value and
that affects all hashes for this flag, so some users start to get a different result than before.

Why would we want that? Imagine we have a flag that enables new feature for the 1% of users. The feature is deployed,
and later you discover a bug that negatively affects user experience. You fix the bug, deploy the change and continue
only to find another bug that can be seen by the same group of poor users. If a user sees a bug after a bug, they can
get disappointed in the service. If you reset the hashes, you define a new random group of 1% users who will be soon
confronted by the bugs. There is a small chance that a user from the first group also joins the second one, but the
majority will be new users. 