# Benchmarks

There 3 benchmarks:

- get flag by HTTP (server-side)
- get flag by GRPC (server-side)
- get a flag value using _FflagClient_ (client-side)

All the input files for server-side evaluation are [here](https://gitlab.com/ittennull/fflag/-/tree/master/benchmark)

Flag for the server-side tests:

```yaml
name: UseNewShoppingCart
enabled: true
variants: !Bool
  - name: 'False'
    value: false
  - name: 'True'
    value: true
default_distributions:
  - 500
  - 500
rules: [ ]
hash_seed: 1698590993729523685
groups: [ ]
```

The testing configuration: AMD Ryzen 9 7940HS, 1 CPU, 16 logical and 8 physical cores .NET SDK
8.0.108

There are two tests for each transport:

- request contains a flag name, FFlag generates a new value for each request
- request contains a flag name and an entityId, FFlag calculates a stable hash value and generates a response for this
  hash

## HTTP

The tool used for this is load testing is [wrk](https://github.com/wg/wrk)

```bash
wrk -c100 -t100 -d30s --latency -s ./wrk_name.lua http://localhost:8080/eval
Running 30s test @ http://localhost:8080/eval
  100 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   229.48us  262.97us   7.20ms   92.05%
    Req/Sec     5.45k   525.89    17.40k    89.53%
  Latency Distribution
     50%  156.00us
     75%  248.00us
     90%  431.00us
     99%    1.41ms
  16300520 requests in 30.10s, 1.86GB read
Requests/sec: 541554.98
Transfer/sec:     63.27MB
```

```bash
wrk -c100 -t100 -d30s --latency -s ./wrk_name_and_entityId.lua http://localhost:8080/eval
Running 30s test @ http://localhost:8080/eval
  100 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   221.18us  242.50us   8.46ms   91.84%
    Req/Sec     5.53k   476.38    11.09k    87.02%
  Latency Distribution
     50%  154.00us
     75%  241.00us
     90%  414.00us
     99%    1.27ms
  16576976 requests in 30.10s, 1.90GB read
Requests/sec: 550719.40
Transfer/sec:     64.60MB
```

## GRPC

The tool is [ghz](https://ghz.sh)

```bash
ghz --config ./ghz_name.json

Summary:
  Count:	1527249
  Total:	29.93 s
  Slowest:	18.79 ms
  Fastest:	0.05 ms
  Average:	1.21 ms
  Requests/sec:	51026.87

Response time histogram:
  0.052  [1]      |
  1.926  [849639] |∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
  3.799  [130243] |∎∎∎∎∎∎
  5.673  [18041]  |∎
  7.546  [1792]   |
  9.419  [211]    |
  11.293 [38]     |
  13.166 [16]     |
  15.040 [6]      |
  16.913 [10]     |
  18.787 [3]      |

Latency distribution:
  10 % in 0.33 ms 
  25 % in 0.58 ms 
  50 % in 0.98 ms 
  75 % in 1.51 ms 
  90 % in 2.32 ms 
  95 % in 3.00 ms 
  99 % in 4.39 ms
```

```bash
ghz --config ./ghz_name_and_entityId.json

Summary:
  Count:	1491446
  Total:	29.94 s
  Slowest:	18.75 ms
  Fastest:	0.05 ms
  Average:	1.25 ms
  Requests/sec:	49813.74

Response time histogram:
  0.053  [1]      |
  1.923  [846849] |∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
  3.793  [132634] |∎∎∎∎∎∎
  5.662  [18196]  |∎
  7.532  [1921]   |
  9.402  [270]    |
  11.271 [83]     |
  13.141 [31]     |
  15.011 [7]      |
  16.881 [5]      |
  18.750 [3]      |

Latency distribution:
  10 % in 0.33 ms 
  25 % in 0.58 ms 
  50 % in 0.99 ms 
  75 % in 1.53 ms 
  90 % in 2.32 ms 
  95 % in 2.98 ms 
  99 % in 4.42 ms
```

If you compare requests per second, the results for grpc are disappointing compared to HTTP. I believe the `ghz` tool
is culpable. When I run `wrk` tool I see about 60% CPU utilization where `wrk` and FFlag take both about 30%. When I
run `ghz`, it takes about 75% of CPU of which FFlag is only 12%. So FFlag is sitting there almost idle

## Client-side evaluation

For client-side C# client I use standard [BenchmarkDotNet](https://github.com/dotnet/BenchmarkDotNet). The source code
is [here](../../clients/csharp/FflagClient/FflagClient.Benchmark)

| Method                          |      Mean |    Error |   StdDev |   Gen0 | Allocated |
|---------------------------------|----------:|---------:|---------:|-------:|----------:|
| RandomWithoutParameters         |  26.76 ns | 0.305 ns | 0.286 ns |      - |         - |
| WithEntityId                    |  52.59 ns | 0.464 ns | 0.387 ns | 0.0143 |     120 B |
| WithEntityIdAndCountryParameter | 110.07 ns | 2.220 ns | 4.779 ns | 0.0516 |     432 B |
| WithCountryParameter            |  79.18 ns | 1.602 ns | 3.009 ns | 0.0372 |     312 B |

`1 s / 27 ns = 37_037_037` operations per second. This staggering number is orders of magnitude bigger than HTTP or
GRPC. Even if we take another bigger delay from the other scenario, the difference is unbeatable