# FflagClient

This is a client-side C# client for FFlag. It evaluates requests right in the process on a client instead of making an
HTTP call to FFlag every time a flag is requested.
The benefit of it is a huge unmatched performance (see [benchmark](./benchmark.md#client-side-evaluation))

## How to use

1. Install and register the package

```bash
dotnet add package FflagClient
```

```csharp
builder.Services.AddFflagClient(options =>
{
    options.ServerUri = new Uri("http://localhost:9090");
    options.UpdateIntervalInSeconds = 20;           // default is 60 seconds
    options.WatchedGroups.Add("this-app-name");     // at least one watched group is required
});
```

You need to point the client to the `Uri` of the FFlag and specify at least one watched group.

2. Inject IFlagEvaluator into a constructor or a handler

```csharp
IFlagEvaluator flagEvaluator
```

3. Call the client

```csharp
flagEvaluator.GetBool(flagName); // just a flag name
flagEvaluator.GetBool(flagName, userId); // flag name and entityId
flagEvaluator.GetBool(flagName, userId, Parameters.Add(countryCode).Add(age)); // with parameters

// other methods are available. Call these depending on the type of a flag
flagEvaluator.GetDouble(...)
flagEvaluator.GetLong(...)
flagEvaluator.GetString(...)
```

## How it works

The client has a background job that queries flag configurations from FFlag every `UpdateIntervalInSeconds`. The server
returns flags updated since the last time. The
configuration is stored in memory.
FFlag can have thousands of flags defined in it, but it's rare that an app needs all of them. Usually only a subset of
flags is queried from an app and the rest of the flags are meant for other apps.

To specify which flag configuration to keep in memory, provide a list of groups to which the flags should belong. A
group is just a string that denotes a set of flags and is defined in FFlag UI:

![](./img/groups.png)

A flag can belong to multiple groups. A flag must have a group to be used in FflagClient.

A group can be a name of an app or a name of a UX flow if the flow goes through multiple services and is queried by all
of them.