# Installation

FFlag is available as a docker image, you can run it locally or deploy, for example, to Azure App Services or kubernetes

The only requirement is MongoDB.

```bash
docker run \
  -e mongodb_connection_string=mongodb://<address> \
  -e RUST_LOG=info \
  -p 8080:8080 \
  -p 9090:9090 \
  registry.gitlab.com/ittennull/fflag
```

Default ports are `8080` (HTTP) and `9090` (Grpc). You can override them with environment variables:

```bash
docker run \
    -e server__grpc_address=0.0.0.0:5000 \
    -e server__http_address=0.0.0.0:6000 ...
```

Available log levels: `error`, `warn`, `info`, `debug`, `trace`

# Horizontal scaling

It's possible to spin up multiple instances of FFlag and connect them to the same MongoDB database. Changes made
by one instance will be visible to others. This allows you to deploy FFlag to kubernetes and
use horizontal pod autoscaling. The changes in the database become visible to other instances in 30 sec by default (
configurable)

# Try it locally

Run this command to start the FFlag

```bash
echo "
services:
  fflag:
    image: registry.gitlab.com/ittennull/fflag
    ports:
    - 9090:9090
    - 8080:8080
    environment:
    - mongodb_connection_string=mongodb://mongo
    - RUST_LOG=info
    depends_on:
    - mongo

  mongo:
    image: mongo
  " | docker compose -f - up
```

Navigate to [http://localhost:8080](http://localhost:8080) to see the UI.

Let's create a flag (you can do the same via the UI):

```bash
curl 'http://localhost:8080/api/flags' \
  -H 'Content-Type: application/yaml' -d '
name: UseNewShoppingCart
enabled: true
variants: !Bool
- name: Old
  value: false
- name: New
  value: true
default_distributions:
- 500
- 500
rules: []
hash_seed: 4018124498564276586
groups: [] '
```

Now we can query the flag:

```bash
curl 'http://localhost:8080/eval' --json '{"name":"UseNewShoppingCart"}'
```