# Creating flags

There are several types of flags: `bool`, `string`, `integer`, `float`.
A type determines the kind of values you can get when you query a flag.

If you select the `bool` type then the values are predefined and cannot be changed, for all other types you need to add
the values yourself.

The flag definitions are stored in the database.

There are a couple of ways to create a flag. First and the easiest one is via UI. Another one is via API, the same api
that is used by the UI itself (see example in the [installation](installation.md#try-it-locally) section).

Additionally, you can export and import flags. It's very useful when you have several environments (test, acceptance,
production) and you want to create the same flag everywhere. You can create it once in one environment, export and
import in the others.

## Distribution and Rules

Distribution defines a percentage of requests that receive a particular flag value. There is a default
distribution, and it can be overridden by rules.

Rule is a distribution that is used when a request satisfies a certain condition. For example:

![example](img/ui_with_rule.png)

The flag "UseNewShoppingCart" has default distribution 90/10% and a rule that says that if the "age" request parameter
is greater than 50 or the country is one from the list use different distribution 0/100%, in other words - always use
the new shopping cart.

You can have as many rules as you like. They are tested one by one starting from the top and the first one that fits is
used to make a response.

Note that you can set a user-friendly name to each rule or use a default one.

## Request parameters and condition operators

A request parameter is just a key-value pair from the request. The details of how to pass such a parameter are in the
next chapters.

The value can be of different types:

- `bool`: `true`, `false`
- `integer`: 0, 1, 2, 44, -123
- `float`: 0.0, -10.5, 100.0 (the difference from an integer is the decimal point)
- `string`: "", "hello", "come with me if you want to live"

Parameter types are separate from a flag type, for example on the picture above the flag type is `bool` and it can have
only two values `true/false` but the parameters have types `integer` (age) and `string` (country).

The type of a parameter is inferred based on the other value of a condition:

```json
age > 50 // age must be an integer
country In ["Netherlands", "Germany", "Spain"] // country must be a string
```

Available operators for conditions:

- `<` - less than: `age < 50`
- `<=` - less than or equal
- `>` - greater than
- `>=` - greater than or equal
- `==` - equal to
- `!=` - not equal to
- `In` - a value is one from an array: `country In ["Netherlands", "Germany", "Spain"]`
- `NotIn` - a values is not part of an array: `five NotIn [1,2,3,4]`

An array can contain strings, integers or floats. All values must be of the same type.

Logical `AND` and `OR` are also available, as well as groups `(...)`:

```json
(age > 50 AND country In ["Netherlands", "Spain"]) OR isPreviewTester == true
```

There is a special parameter `$EntityId` used for stickiness, its type is always a `string`. More about it [here](entityId.md). 

```json
$EntityId In ["Frodo", "Gandalf", "Aragorn's horse"]
```

An application defines what parameters can affect the returned flag value.
If a request doesn't have a parameter name that is used in a condition this condition is considered not fulfilled.
