# FFlag

FFlag lets you define a set of feature flags using web UI and provides an API to query the flags from your applications.

It resembles a simple `if`-condition in code only moved to an external system, hence achieving de-coupling decision
points from decision logic

You can read more about the concept of feature flags here:

- [wikipedia](https://en.wikipedia.org/wiki/Feature_toggle)
- [very detailed explanation from Martin Fowler](https://martinfowler.com/articles/feature-toggles.html)

### Overview

You create flags using UI or API, they are stored in MongoDB database

![overview](img/overview.svg)

### Example

Let's create a simple flag determining if a user is shown an old shopping cart or a new shiny one.

In the UI of FFlag you provide a name for the flag, define two flag values with the names "Old" and "New" (can be
anything) and set a default distribution. The new shopping cart is presented to 10% of all users.

![ui](img/ui_example.png)

Query the flag with the command:

```bash
curl http://localhost:8080/eval --json '{"name":"UseNewShoppingCart"}'
```