Feature flag system: flags, dynamic configuration and A/B testing. It has a built-in UI to create flags as well as HTTP and Grpc APIs to query the flags.

Read more in the [documentation](https://ittennull.gitlab.io/fflag)

An example of the UI:

![example](docs/src/img/ui_with_rule.png)